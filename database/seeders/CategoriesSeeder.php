<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('categories')->insert([
            [
                'description' => 'Deporte',
            ],
            [
                'description' => 'Salud',
            ],
            [
                'description' => 'Tecnología',
            ]
        ]);
    }
}
