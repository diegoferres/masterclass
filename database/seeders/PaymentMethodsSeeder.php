<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PaymentMethodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('payment_methods')->insert([
            [
                'description' => 'Tarjeta (online)',
                'active' => 1
            ],
            [
                'description' => 'Tarjeta guardada (online)',
                'active' => 0
            ],
            [
                'description' => 'Transferencia',
                'active' => 1
            ]
        ]);
    }
}
