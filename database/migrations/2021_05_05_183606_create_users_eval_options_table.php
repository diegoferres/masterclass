<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersEvalOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_eval_options', function (Blueprint $table) {
            $table->id('id');
            $table->foreignId('option_id')->references('id')->on('options');
            $table->foreignId('users_evaluation_id')->nullable()->references('id')->on('user_evaluations')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_eval_options');
    }
}
