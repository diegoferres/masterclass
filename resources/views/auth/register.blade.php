@extends('front.layouts.app')
@section('header')

@stop
@section('content')
    <!-- Forms23 block -->
    <section class="mc-forms-23">
        <div id="forms23-block">
            <!---728x90--->

            <div class="wrapper">
                <div class="logo1">

                    <a class="brand-logo" href="#index.html">
                        <img src="/front/assets/images/logo_white.png" alt="Masquelier logo" title="masquelier logo"
                             style="height:150px;"/>
                    </a>
                </div>
                <div class="d-grid forms25-grids">

                    <div class="form23">
                        <h6>Registrese con una nueva cuenta</h6>
                        <br>
                        @if ($errors->all())
                            <div class="alert alert-warning" role="alert">
                                <h5><i class="fa fa-warning"></i> Tienes estos errores</h5>
                                @foreach ($errors->all() as $key => $error)

                                    <p class="mb-0">{{ $key+1 }}. {{ $error }}</p>
                                @endforeach
                            </div>
                        @endif

                        <form action="{{ route('register') }}" method="POST">
                            @csrf
                            <input value="{{ old('name') }}" type="text" name="name" placeholder="Nombre"
                                   class="is-invalid" required="required">
                            <input value="{{ old('email') }}" type="email" name="email" placeholder="Email"
                                   required="required">
                            <input type="password" name="password" placeholder="Contraseña" required="required">
                            <input type="password" name="password_confirmation" placeholder="Confirmar contraseña"
                                   required="required">

                            <button type="submit">Crear cuenta</button>
                        </form>
                        <h5>¡Ya tengo una cuenta! <a href="{{ route('login') }}">Iniciar sesión</a></h5>
                    </div>

                </div>
                <br>
                <div class="text-center">
                    <a href="{{ route('home') }}" class="btn btn-primary">Volver al inicio</a>
                </div>
            </div>
            <!---728x90--->
        </div>
    </section>
    <!-- Forms23 block -->
@endsection
