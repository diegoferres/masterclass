@extends('front.layouts.app')
@section('header')

@stop
@section('content')
    <!-- Forms23 block -->
    <section class="mc-forms-23">
        <div id="forms23-block">
            <!---728x90--->

            <div class="wrapper">
                <div class="logo1">

                    <a class="brand-logo" href="#index.html">
                        <img src="/front/assets/images/logo_white.png" alt="Masquelier logo" title="masquelier logo"
                             style="height:150px;"/>
                    </a>
                </div>
                <div class="d-grid forms23-grids">
                    <div class="frm-tp">
                        <div class="form23-text">
                            <a href="{{ route('home') }}" class="actionbg2 btn">Volver al inicio</a>
                        </div>
                    </div>
                    <div class="form23">
                        <h6>Ingresar contraseña nueva</h6>
                        @if (session('status'))
                            <div class="mb-4 font-medium text-sm text-green-600">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="POST" action="{{ route('password.update') }}">
                            @csrf
                            <input type="email" name="email" placeholder="Correo" value="{{ request()->email }}"
                                   class="@error('email') is-invalid @enderror" required="required">
                            @error('email')
                            <span class="invalid-feedback">
                                        {{ $message }}
                                    </span>
                            @enderror
                            <input autofocus type="password" name="password" placeholder="Contraseña nueva"
                                   class="@error('password') is-invalid @enderror" required="required">
                            @error('password')
                            <span class="invalid-feedback">
                                        {{ $message }}
                                    </span>
                            @enderror
                            <input type="password" name="password_confirmation" placeholder="Confirmar contraseña"
                                   class="form-control" required="required">
                            <button type="submit">Guardar</button>
                        </form>
                    </div>
                </div>
            </div>
            <!---728x90--->
        </div>
    </section>
    <!-- Forms23 block -->
@endsection
