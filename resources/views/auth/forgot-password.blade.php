
@extends('front.layouts.app')
@section('header')

@stop
@section('content')
    <!-- Forms23 block -->
    <section class="mc-forms-23">
        <div id="forms23-block">
            <!---728x90--->

            <div class="wrapper">
                <div class="logo1">

                    <a class="brand-logo" href="#index.html">
                        <img src="/front/assets/images/logo_white.png" alt="Masquelier logo" title="masquelier logo" style="height:150px;" />
                    </a>
                </div>
                <div class="d-grid forms25-grids">

                    <div class="form23">
                        <h6>Recuperar contraseña</h6>
                        <br>
                        @if (session('status'))
                            <div class="mb-4 font-medium text-sm text-green-600">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf
                            <input autofocus type="email" name="email" placeholder="Correo" value="{{ old('email') }}" class="@error('email') is-invalid @enderror" required="required">
                            @error('email')
                            <span class="invalid-feedback">
                                        {{ $message }}
                                    </span>
                            @enderror
                            <button type="submit">Solicitar</button>

                        </form>

                    </div>

                </div>
                <br>
                <div class="text-center">
                <a href="{{ route('home') }}" class="btn btn-primary">Volver al inicio</a>
                </div>
            </div>
            <!---728x90--->
        </div>
    </section>
    <!-- Forms23 block -->
@endsection
