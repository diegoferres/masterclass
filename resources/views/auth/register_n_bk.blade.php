@extends('front.layouts.app')
@section('header')

@stop
@section('content')
    <!-- Forms23 block -->
    <section class="mc-forms-23">
        <!---728x90--->

        <div id="forms23-block">
            <div class="wrapper">
                <div class="logo1">

                    <a class="brand-logo" href="#index.html">
                        <img src="/front/assets/images/logo_white.png" alt="Masquelier" title="Masquelier"
                             style="height:150px;"/>
                    </a>
                </div>
                <div class="d-grid forms23-grids">
                    <div class="frm-tp">
                        <div class="form23-text">
                            <h6>Regístrese con las redes sociales</h6>
                            <a href="#facebook">
                                <div class="signin facebook">
                                    <span class="fa fa-facebook" aria-hidden="true"></span>
                                    <p class="action">Facebook</p>
                                </div>
                            </a>
                            <a href="#google-plus">
                                <div class="signin google-plus">
                                    <span class="fa fa-google-plus" aria-hidden="true"></span>
                                    <p class="action">Google</p>
                                </div>
                            </a>
                            {{--<a href="#twitter">
                                <div class="signin twitter">
                                    <span class="fa fa-twitter" aria-hidden="true"></span>
                                    <p class="action">Twitter</p>
                                </div>
                            </a>
                            <a href="#linkedin">
                                <div class="signin linkedin">
                                    <span class="fa fa-linkedin" aria-hidden="true"></span>
                                    <p class="action">Linkedin</p>
                                </div>
                            </a>--}}
                            <a href="{{ route('home') }}" class="actionbg2 btn">Volver al inicio</a>
                        </div>
                    </div>
                    <div class="form23">
                        <h6>Registrese con una nueva cuenta</h6>


                        <form action="{{ route('register') }}" method="POST">
                            @csrf
                            <input type="text" name="name" placeholder="Nombre" required="required">
                            <input type="email" name="email" placeholder="Email" required="required">
                            <input type="password" name="password" placeholder="Contraseña" required="required">
                            <input type="password" name="password_confirmation" placeholder="Confirmar contraseña"
                                   required="required">

                            <button type="submit">Crear cuenta</button>
                        </form>
                        <h5>¡Ya tengo una cuenta! <a href="{{ route('login') }}">Iniciar sesión</a></h5>

                    </div>

                </div>
            </div>

        </div>
        <!---728x90--->

    </section>
    <!-- Forms23 block -->
@endsection
