@extends('front.layouts.app')
@section('header')

@stop
@section('content')
    <!-- Forms23 block -->
    <section class="mc-forms-23">
        <div id="forms23-block">
            <!---728x90--->

            <div class="wrapper">
                <div class="logo1">

                    <a class="brand-logo" href="#index.html">
                        <img src="/front/assets/images/logo_white.png" alt="Masquelier logo" title="masquelier logo" style="height:150px;" />
                    </a>
                </div>
                <div class="d-grid forms23-grids">
                    <div class="frm-tp">
                        <div class="form23-text">
                            <h6>Iniciar sesión con redes sociales</h6>
                            <a href="#facebook">
                                <div class="signin facebook">
                                    <span class="fa fa-facebook" aria-hidden="true"></span>
                                    <p class="action">Facebook</p>
                                </div>
                            </a>
                            <a href="#google-plus">
                                <div class="signin google-plus">
                                    <span class="fa fa-google-plus" aria-hidden="true"></span>
                                    <p class="action">Google</p>
                                </div>
                            </a>
                            {{--<a href="#twitter">
                                <div class="signin twitter">
                                    <span class="fa fa-twitter" aria-hidden="true"></span>
                                    <p class="action">Twitter</p>
                                </div>
                            </a>
                            <a href="#linkedin">
                                <div class="signin linkedin">
                                    <span class="fa fa-linkedin" aria-hidden="true"></span>
                                    <p class="action">Linkedin</p>
                                </div>
                            </a>--}}
                            <a href="index.html" class="actionbg2 btn">Volver al inicio</a>
                        </div>
                    </div>
                    <div class="form23">
                        <h6>Inicie sesión con la cuenta del sitio</h6>

                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <input type="email" name="email" placeholder="Email" value="{{ old('email') }}" class="@error('email') is-invalid @enderror" required="required">
                            @error('email')
                            <span class="invalid-feedback">
                                        {{ $message }}
                                    </span>
                            @enderror

                            <input type="password" name="password" placeholder="Contraseña" class="@error('password') is-invalid @enderror" required="required">
                            <a href="{{ route('password.request') }}">Olvidé mi contraseña</a>
                            <button type="submit">Iniciar Sesión</button>
                        </form>
                        <h5>¿Aún no tienes una cuenta? <a href="{{ route('register') }}">Registrate ahora</a></h5>
                    </div>
                </div>
            </div>
            <!---728x90--->

        </div>
    </section>
    <!-- Forms23 block -->
@endsection
