<div>
    {{-- Nivel 1 --}}
    @foreach ($lesson->comments as $comment)
        @if (!$comment->parent)
        <div
            class="[ panel panel-default ] panel-google-plus m-2 comments-respuesta   mt-0 mr-0 mb-0 ">
            <div
                class="panel-heading ml-5 mt-0 mr-0 mb-0 pl-2   rounded   "
                style="background: #EEEEEE"> {{--  comments-doc--}}
                <img class=" rounded-circle pull-left"
                     style="position:absolute; left: -2px; top: 0px" height="40px"
                     src="https://ui-avatars.com/api/?name={{ $comment->user->name }}"
                     alt="Mouse0270"/>
                <div class="panel-body  ">
                    <div class="text-justify p-2 small ">
                        {{ $comment->comment }}
                    </div>
                </div>
            </div>
            <div class="panel-body ml-4 pl-4">
                <span class="p-1 ">{{ $comment->user->name }}</span> |
                <a style="color: blue; cursor: pointer" wire:click="replyComment({{ $comment->id }})">Responder</a> |
                @if ($comment->user->id == Auth::id())
                    <a style="color: blue; cursor: pointer" wire:click="destroyComment({{ $comment->id }})">
                        <span wire:loading.remove wire:target="destroyComment">Eliminar</span>
                        <span wire:loading wire:target="destroyComment">Eliminando</span>
                    </a> |
                @endif
                <span class="text-gray"
                      style="color: #999">comentado el {{ date('d/m/Y H:s', strtotime($comment->created_at)) }}</span>
            </div>
        </div>
        @else
            <div
                class="[ panel panel-default ] panel-google-plus m-2 comments-respuesta ml-5   mt-0 mr-4 mb-0 ">
                <div
                    class="panel-heading ml-5 mt-0 mr-0 mb-0 pl-2   rounded   "
                    style="background: #EEEEEE"> {{--  comments-doc--}}
                    <img class=" rounded-circle pull-left"
                         style="position:absolute; left: -2px; top: 0px" height="40px"
                         src="https://ui-avatars.com/api/?name={{ $comment->user->name }}"
                         alt="Mouse0270"/>
                    <div class="panel-body  ">
                        <div class="text-justify p-2 small ">
                            {{ $comment->comment }}
                        </div>
                    </div>
                </div>
                {{--<div class="panel-body ml-4 pl-4">
                                        <span class="p-1 font-weight-bolder text-success">Doc. Martinez <i
                                                class="fa fa-star"></i></span> |
                    <span class="text-gray" style="color: #999">comentado el 27, marzo 2021</span>
                </div>--}}
                <div class="panel-body ml-4 pl-4">

                        @role('admin')
                            <span class="p-1 font-weight-bolder text-success">Doc. Martinez <i
                                    class="fa fa-star"></i></span>
                        @else
                        <span class="p-1 ">
                            {{ $comment->user->name }}
                        </span> |
                        @endrole
                    <a style="color: blue; cursor: pointer" wire:click="replyComment({{ $comment->id }})">Responder</a> |
                    @if ($comment->user->id == Auth::id())
                        <a style="color: blue; cursor: pointer" wire:click="destroyComment({{ $comment->id }})">Eliminar</a> |
                    @endif
                    <span class="text-gray"
                          style="color: #999">comentado el {{ date('d/m/Y H:s', strtotime($comment->created_at)) }}</span>
                </div>
            </div>
        @endif
    @endforeach
    <div class="[ panel panel-default ] panel-google-plus">
        <div
            class="[ panel panel-default ] panel-google-plus @if($commentOpen) panel-google-plus-show-comment @endif">

            <div class="panel-body">
                <p>Escribe un comentario, consulta o emite una
                    opinion y presiona ENTER</p>
            </div>
            <div class="panel-footer">
                <div class="input-placeholder" wire:click="$toggle('commentOpen')">Dejar comentario...</div>
            </div>
            <div class="panel-google-plus-comment">
                <img class="rounded-circle"
                     height="45px"
                     src="https://ui-avatars.com/api/?name={{ Auth::user()->name }}"
                     alt="User Image"/>
                @if ($commentResponse)
                    Responder a {{ $commentResponse->user->name }} {{ $parentComment }}
                @endif
                <div class="panel-google-plus-textarea">
                    <textarea @if ($commentOpen) autofocus @endif rows="3" wire:keydown.enter="saveComment" wire:model="commentInput"></textarea>
                    <button wire:click="saveComment"
                            class="[ btn btn-success disabled ]">
                        Comentar
                    </button>
                    <button type="reset" wire:click="cancelReply()" class="[ btn btn-default ]">
                        Cancelar
                    </button>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>


        {{--<div class="panel-google-plus-comment">
            <img class="rounded-circle"
                 src="https://lh3.googleusercontent.com/uFp_tsTJboUY7kue5XAsGA=s46"
                 alt="User Image"/>
            <div class="panel-google-plus-textarea">
                <textarea wire:model="commentInput" rows="4"></textarea>
                <button type="submit"
                        class="[ btn btn-success disabled ]">
                    Comentar
                </button>
                <button type="reset" class="[ btn btn-default ]">
                    Cancel
                </button>
            </div>
            <div class="clearfix"></div>
        </div>--}}
    </div>
</div>
