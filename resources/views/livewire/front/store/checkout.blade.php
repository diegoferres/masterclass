<div>
    <section class="mc-ecom-cart">

        <div class="wrapper ">

            <h1>Checkout</h1>
            <div class="shopping-cart m-5">
                <div class="column-labels">
                    <label class="product-image">Items</label>
                    <label class="product-details">Curso</label>
                    <label class="product-price">Precio</label>
                    <label class="product-price">Descuento</label>
                    <label class="product-line-price">Total</label>
                </div>
                <!--LISTA-->
                <div class="product">
                    <div class="product-image">
                        <a href="details_course.html"><img src="/front/assets\images/course3.png" alt=""
                                                           class="img-responsive"></a>
                    </div>
                    <div class="product-details">
                        <div class="product-title"><a href="details_course.html">{{ $course->title }}</a></div>

                    </div>
                    <div class="product-price">{{ number_format($course->price, 0, '', '.') }}</div>
                    <div class="product-quantity">
                        0%
                    </div>
                    <div class="product-line-price">{{ number_format($course->price, 0, '', '.') }}</div>
                </div>
                <!--fin LISTA-->
                <div class="totals">
                    <div class="totals-item">
                        <label>Subtotal</label>
                        <div class="totals-value"
                             id="cart-subtotal">{{ number_format($course->price, 0, '', '.') }}</div>
                    </div>
                    <div class="totals-item">
                        <label>IVA(10%)</label>
                        <div class="totals-value"
                             id="cart-tax">{{ number_format($course->price / 11, 0, '', '.') }}</div>
                    </div>

                    <div class="totals-item totals-item-total">
                        <label>Total</label>
                        <div class="totals-value" id="cart-total">{{ number_format($course->price, 0, '', '.') }}</div>
                    </div>
                </div>
                <h3>Método de pago</h3>
                <div class="row">
                    <div class="col-md-4 mt-2">
                        @foreach ($paymentMethods as $method)
                            <label class="mt-2">
                                <input name="payment_method" type="radio" value="{{$method->id}}" wire:model="payment_method" >
                                {{$method->description}}
                            </label>
                            <br>
                        @endforeach
                        {{--<label class="mt-2">
                            <input name="payment_method" type="radio" value="1" wire:model="payment_method" >
                            Tarjeta de crédito (Pago online)
                        </label>
                        <label class="mt-2">
                            <input name="payment_method" type="radio" value="2" wire:model="payment_method" >
                            Transferencia
                        </label>--}}
                        @if ($payment_method == 2)
                            <br>
                            <label for="">Nro. Comprobante</label>
                            <input type="text" class="form-control @error('voucher') is-invalid @enderror" wire:model="voucher">
                            @error('voucher')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        @endif
                    </div>
                </div>
                <div class="form-group  text-right">
                    <button wire:click="finish" class="btn btn-outline-success btn-lg">Pagar</button>
                    {{--<a href="{{ route('order.payment.insert', $course->id) }}" class="btn btn-outline-success btn-lg">Pagar</a>--}}
                </div>
            </div>
        </div>

    </section>
</div>
