<div>
    <div class="card-body" id="#beginEval">
        <h5 class="card-title">Evaluación del módulo Modulo 1 <h6
                class="card-subtitle mb-2 text-muted pt-2">(Total
                puntos: {{ $eval->items->sum('points') }})</h6></h5>
        <p class="card-text text-justify">Marca la respuesta correcta de cada item</p>
        @if ($evalEnded)
            <p style="color: green" class="card-text text-justify pull-right mr-3"><strong>PUNTOS
                    OBTENIDOS: {{ $points ?? 0 }}</strong></p>
            <br>
            <div>
                <button wire:click="resetEvaluation" class="btn btn-success pull-right mr-3">Repetir evaluación</button>
                @if ($eval->module->next())
                    <a href="{{ route('course.lesson', [$eval->module->course->slug, $eval->module->next()->lessons->first()->slug]) }}" class="btn btn-success pull-right mr-3">Ir al siguiente módulo</a>
                @else
                    <a href="{{ route('home') }}" class="btn btn-success pull-right mr-3">Ir al curso</a>
                @endif
            </div>
        @endif
        <br>

        @if ($error)
            <div class="alert alert-warning" role="alert">
                Por favor, responde todos los items
            </div>
        @endif

        @foreach ($eval->items as $itemKey => $item)
            {{-- Item de evaluacion--}}
            <div class="col-12 mt-3 mb-3 ">
                <div class="card rounded mb-2">
                    <div class="card-body  m-0 pb-0 pt-0 ">
                        <div class="row">
                            <div class="col-sm-10 pt-3 pb-2">
                                <h5>[{{ $itemKey+1 }}] {{ $item->text }}
                                    @if ($evalEnded)
                                        @if ($item->options()->where(['id' => $items[$item->id], 'is_answer' => true])->first())
                                            <i style="color: green" class="fa fa-check"></i>
                                        @else
                                            <i style="color: red" class="fa fa-close"></i>
                                        @endif
                                    @endif</h5>

                                <p>{{ $item->points }} puntos</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body  m-0 pb-0 pt-0 ">
                    @foreach ($item->options as $optionKey => $option)
                        <div class="row">
                            <div class="col-sm-12">
                                <label @if ($evalEnded)
                                       @if ($option->is_answer) style="color: green"
                                       @else style="color: red"
                                       @endif
                                       @endif for="itemOption_{{ $itemKey.$optionKey }}">
                                    <input @if ($evalEnded) disabled @endif required
                                    wire:model="items.{{ $item->id }}"
                                           {{--wire:click="$set('items.{{ $itemKey }}.option', {{ $option->id }})"--}}
                                           style="margin-right: 0px" type="radio" value="{{ $option->id }}"
                                           id="itemOption_{{ $itemKey.$optionKey }}"
                                           name="itemOption_{{ $itemKey }}"
                                    >
                                    {{ $option->text }}</label>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            {{-- FIN Item de evaluacion--}}
        @endforeach
    </div>
    <div class="card-footer">
        @if ($confirm && !$evalEnded)
            <button wire:click="$toggle('confirm')" class="btn btn-warning pull-right mb-2 ml-2">Cancelar</button>
            <button wire:click="saveEvaluation" class="btn btn-info pull-right mb-2">¿Estás seguro/a?</button>
        @elseif (!$confirm && !$evalEnded)
            <button wire:click="$toggle('confirm')" class="btn btn-success pull-right mb-2">Finalizar evalución</button>
        @endif
    </div>

    @push('livewire_scripts')
        <script>
            window.addEventListener('GoToTop',function(e){
                $("html, body").animate({ scrollTop: 0 }, "fast");
            })

        </script>
    @endpush
</div>
