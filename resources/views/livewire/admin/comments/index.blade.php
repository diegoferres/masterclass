<div>
    <div class="table-responsive">
        <div class="row">
            <div class="col-sm-12 col-md-12">

            </div>
            <div class="col-sm-12 col-md-6">
                <div id="order-listing_filter" class="dataTables_filter">
                    <label>
                        <input type="search"
                               wire:model="search"
                               class="form-control"
                               placeholder="Buscar"
                               aria-controls="order-listing">
                    </label>
                </div>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>Comentario</th>
                <th>Respuestas</th>
                <th>Usuario</th>
                <th>Lección</th>
                <th>Curso</th>
                <th>Fecha</th>
                <th>Acciones</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($comments as $comment)
                <tr >
                    <td class="font-weight-bold">
                        {{ $comment->id }}
                    </td>
                    <td class="text-muted">
                        {{ $comment->comment }}
                    </td>
                    <td class="text-muted">

                        <table>
                            @if ($comment->replies)
                                @foreach ($comment->replies as $rp)
                                    <tr>
                                        <td>{{ $rp->comment }}</td>
                                        <td>{{ $rp->user->email }}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                        {{--@if ($comment->replies)
                            @foreach ($comment->replies as $rp)
                                <div class="middlecenter">{{ $rp->comment }}</div>
                            @endforeach
                        @endif--}}

                        {{--<div class="lowercenter">Datapoint3</div>--}}
                        {{-- For reply --}}
                        @if ($reply == $comment->id)
                            <input wire:model="reply_comment" wire:keydown.enter="saveComment({{ $comment->id }})" type="text" class="form-control form-control-sm" placeholder="Responde y presiona ENTER">
                        @endif
                    </td>
                    <td class="text-muted">
                        {{ $comment->user->name }}
                        <br>
                        {{ $comment->user->email }}
                    </td>
                    <td class="text-muted">
                        {{ $comment->lesson->title }}
                    </td>
                    <td class="text-muted">
                        {{ $comment->lesson->module->course->title }}
                    </td>
                    <td class="text-muted">
                    {{ $comment->created_at }}
                    </td>
                    <td>
                        @if ($reply == $comment->id)
                            <button wire:click="$set('reply', null)" class="btn btn-outline-danger btn-xs"><i
                                    class="fas fa-close"> Cancelar</i></button>
                        @else
                            <button wire:click="deleteComment({{ $comment->id }})" class="btn btn-outline-danger btn-xs"><i
                                    class="fas fa-trash"></i></button>
                            <button wire:click="$set('reply', {{ $comment->id }})" class="btn btn-outline-info btn-xs"><i
                                    class="fas fa-reply"></i></button>
                        @endif

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
    {{ $comments->links() }}
</div>
