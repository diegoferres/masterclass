<div>
    <div class="table-responsive">
        <div class="row">
            <div class="col-sm-12 col-md-12">

            </div>
            <div class="col-sm-12 col-md-6">
                <div id="order-listing_filter" class="dataTables_filter">
                    <label>
                        <input type="search"
                               wire:model="search"
                               class="form-control"
                               placeholder="Buscar"
                               aria-controls="order-listing">
                    </label>
                </div>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Correo</th>
                <th>Roles</th>
                <th>Activado el</th>
                <th>Creado el</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($users as $user)
                <tr >
                    <td class="font-weight-bold">
                        {{ $user->id }}
                    </td>
                    <td class="text-muted">
                        {{ $user->name }}
                    </td>
                    <td class="text-muted">
                        {{ $user->email }}
                    </td>
                    <td class="text-muted">
                        <table>
                            @if ($user->roles)
                                @foreach ($user->roles as $role)
                                    <tr>
                                        <td>{{ $role->name }}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                    </td>
                    <td class="text-muted">
                        {{ $user->email_verified_at }}
                        <br>
                        {{ $user->created_at }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
    {{ $users->links() }}
</div>
