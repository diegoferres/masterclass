<div>
    <div class="card-body">
        <h4 class="card-title">
            <i class="fas fa-newspaper"></i>
            Ordenes
            {{--<a href="{{ route('admin.categories.create') }}" class="fa-pull-right btn btn-info btn-xs"><i class="fas fa-plus"></i></a>--}}
        </h4>
        <div class="row">
            <div class="col-sm-12 col-md-6">

            </div>
            <div class="col-sm-12 col-md-6">
                <div id="order-listing_filter" class="dataTables_filter">
                    <label>
                        <input type="search"
                               wire:model="search"
                               class="form-control"
                               placeholder="Buscar"
                               aria-controls="order-listing">
                    </label>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Curso</th>
                    <th>Alumno</th>
                    <th>Método Pago</th>
                    <th>Transacción</th>
                    <th>Pagado</th>
                    <th>Fecha</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($orders as $order)
                    <tr>
                        <td class="font-weight-bold">
                            {{ $order->id }}
                        </td>
                        <td class="text-muted">
                            {{ $order->course->title }}
                        </td>
                        <td class="text-muted">
                            {{ $order->user->name }}
                        </td>
                        <td class="text-muted">
                            {{ $order->payment_method->description ?? '' }}
                        </td>
                        <td class="text-muted">
                            @if ($order->payment && $order->payment_method_id == 1)
                                <table>
                                    <tr>
                                        <th>Estado</th>
                                        <td>{{ json_decode($order->payment)->response_description }}</td>
                                    </tr>
                                    <tr>
                                        <th>Ticket</th>
                                        <td>{{ json_decode($order->payment)->ticket_number }}</td>
                                    </tr>
                                    <tr>
                                        <th>Monto</th>
                                        <td>{{ number_format(json_decode($order->payment)->amount) }}</td>
                                    </tr>
                                </table>
                            @elseif ($order->payment_method_id == 2)
                                Comprobante Nro.
                                <br>
                                {{ $order->voucher }}
                            @endif
                        </td>
                        <td class="text-muted">
                            <label class="badge badge-{{ $order->paid ? 'success' : 'danger' }} badge-pill">{{ $order->paid ? 'SI' : 'NO' }}</label>
                        </td>
                        <td class="text-muted">
                            {{ $order->created_at }}
                        </td>
                        <td>
                            @if ($order->payment_method_id == 2)
                                @if (!$order->paid)
                                    <button class="btn btn-info btn-xs" wire:click="paid({{ $order->id }})">Aprobar</button>
                                @else
                                    <button class="btn btn-warning btn-xs" wire:click="paid({{ $order->id }})">Deshacer</button>
                                @endif
                            @endif
                        </td>
                        {{--<td>
                            {!! Form::open(['route' => ['admin.categories.destroy', $category->id], 'method' => 'DELETE']) !!}
                                <button type="submit" class="btn btn-outline-danger btn-xs"><i class="fas fa-trash"></i></button>
                            {!! Form::close() !!}
                            <a href="{{ route('admin.categories.edit', $category->id) }}" class="btn btn-outline-info btn-xs"><i class="fas fa-pencil-alt"></i></a>
                        </td>--}}
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $orders->links() }}
        </div>
    </div>
</div>
