<div>
    <div class="table-responsive">
        <div class="row">
            <div class="col-sm-12 col-md-6">

            </div>
            <div class="col-sm-12 col-md-6">
                <div id="order-listing_filter" class="dataTables_filter">
                    <label>
                        <input type="search"
                               wire:model="search"
                               class="form-control"
                               placeholder="Buscar"
                               aria-controls="order-listing">
                    </label>
                </div>
            </div>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Correo</th>
                <th>Cursos</th>
                <th>Registrado el</th>
                <th>Acciones</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($students as $student)
                <tr>
                    <td class="font-weight-bold">
                        {{ $student->id }}
                    </td>
                    <td class="text-muted">
                        {{ $student->name }}
                    </td>
                    <td class="text-muted">
                        {{ $student->email }}
                    </td>
                    <td class="text-muted">
                        @foreach ($student->courses as $course)
                            <span class="badge badge-outline-info">{{ $course->title }}</span>
                        @endforeach

                        {{--{{ $student->orders }}--}}
                    </td>
                    <td class="text-muted">
                        {{ $student->created_at }}
                    </td>
                    <td>
                        {!! Form::open(['route' => ['admin.courses.destroy', $student->id], 'method' => 'DELETE']) !!}
                        <button type="submit" class="btn btn-outline-danger btn-xs"><i
                                class="fas fa-trash"></i></button>
                        {!! Form::close() !!}
                        {{--<a href="{{ route('admin.courses.edit', $student->id) }}"
                           class="btn btn-outline-info btn-xs"><i class="fas fa-pencil-alt"></i></a>--}}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
    {{ $students->links() }}
</div>
