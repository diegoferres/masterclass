<div>
    <div class="row">

        <div class="col-12 col-md-8 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">

                    <h4 class="card-title">Editar curso</h4>
                    <p class="card-description">
                        Edita la información del curso
                    </p>
                    {!! Form::open(['route' => ['admin.courses.update', $course->id], 'method' => 'PATCH', 'files' => true]) !!}
                    <div class="form-group row">
                        <div class="col-sm-7">
                            <label>Título (requerido *):</label>
                            {!! Form::text('course_title', old('course_title'), ['wire:model' => 'course_title','class' => 'form-control form-control-sm'.($errors->has('course_title') ? ' is-invalid': '')]) !!}
                            @error('title')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="col-sm-3">
                            <label>Imagen curso (requerido *):</label>

                            {!! Form::file('course_resourceNew', ['wire:model' => 'course_resourceNew','class' => 'form-control form-control-sm'.($errors->has('course_resourceNew') ? ' is-invalid': '')]) !!}
                            @error('course_resourceNew')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="col-sm-2">
                            {{--<div wire:loading wire:target="course_resourceNew">
                                Cargando imagen...
                            </div>--}}

                            @if($course_resourceNew)
                                <img width="100px" src="{{ $course_resourceNew->temporaryUrl() }}" alt="">
                            @else
                                <img {{--wire:loading.remove--}} width="100px"
                                     src="{{ asset('storage/'.$course->intro_resource) }}" alt="">
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <div class="form-group" wire:ignore>
                                <label for="exampleTextarea1">Descripción (requerido *)</label>
                                {!! Form::textarea('course_description', old('course_description') ?? isset($course) ? $course->description : '',
                                ['id' => 'course_description','wire:model' => 'course_description','rows' => '3','class' => 'form-control'.($errors->has('course_description') ? ' is-invalid': '')]) !!}
                                @error('course_description')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <input type="submit" class="btn btn-primary mr-2 pull-right" name="save_course"
                           value="Guardar cambios">
                    <a href="{{ route('admin.courses.index') }}" class="btn btn-light">Cancelar</a>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <div class="col-12 col-md-4 grid-margin stretch-card">
            <div class="col-12 col-md-12 short-div grid-margin">
                {!! Form::open(['route' => ['admin.courses.update', $course->id], 'method' => 'PATCH']) !!}
                <div class="card">
                    <div class="card-body">
                        <input type="submit" class="btn btn-success" name="save_options" value="Guardar cambios">
                        {{--<button class="btn btn-success">Guardar cambios</button>--}}
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">

                        <h4 class="card-title">Opciones</h4>
                        <p class="card-description">
                            Edita la información del curso
                        </p>

                        <div class="form-group row" wire:ignore>
                            <label>Categorías (requerido *):</label>
                            {!! Form::select('categories[]',
                                $categories,
                                isset($course) ? $course->categories->pluck('id') : old('categories'),
                                ['class' => 'js-example-basic-multiple form-control'.($errors->has('categories') ? ' is-invalid': '') , 'multiple', 'style' => 'width:100%']) !!}
                            @error('categories')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group row">

                            <label>Fecha/Hora Inicio (opcional):</label>
                            {{ Form::input('dateTime-local', 'datetime_start',
                                isset($course) ? strftime('%Y-%m-%dT%H:%M', strtotime($course->datetime_start)) : old('datetime_start'), ['wire:model' => 'course_datetime_start','class' => 'form-control form-control-sm'.($errors->has('datetime_start') ? ' is-invalid': '')]) }}
                            @error('datetime_start')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror

                        </div>
                        <div class="form-group row">

                            <label>Fecha/Hora Fin (opcional):</label>
                            {{ Form::input('dateTime-local', 'datetime_end',
                            isset($course) ? strftime('%Y-%m-%dT%H:%M', strtotime($course->datetime_end)) : old('datetime_end'),
                            ['wire:model' => 'course_datetime_end','class' => 'form-control form-control-sm'.($errors->has('datetime_end') ? ' is-invalid': '')]) }}
                            @error('datetime_end')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <label>Precio (requerido *):</label>
                            {!! Form::number('course_price', null, ['wire:model' => 'course_price','class' => 'form-control form-control-sm'.($errors->has('course_price') ? ' is-invalid': '')]) !!}
                            @error('course_price')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>

        <div class="col-md-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Módulos</h4>
                    <div class="add-items d-flex">
                        <input type="text" wire:model="moduleTitle" class="form-control todo-list-input"
                               placeholder="Título del módulo">
                        <button class="btn btn-primary font-weight-bold" wire:click="addModule()">Agregar</button>
                    </div>
                    <div class="list-wrapper">
                        <div wire:sortable="sortModules" id="profile-list-left" class="py-2">
                            @foreach ($course->modules->sortBy('sort') as $module)
                                <div wire:sortable.item="{{ $module->id }}" wire:key="module-{{ $module->id }}"
                                     class="card rounded mb-2">
                                    <div class="card-body p-3">
                                        <div class="media">

                                            <i wire:sortable.handle class="fas fa-grip-vertical fa-2x mr-3"></i>
                                            <div class="media-body">
                                                <h6 class="mb-1">{{ $module->title }}</h6>
                                            </div>
                                            <button wire:click="deleteModule({{ $module->id }})"
                                                    class="btn btn-xs btn-outline-danger">
                                                <i class="fa fa-times-circle"></i>
                                            </button>
                                            <button wire:click="selectModule({{ $module->id }})"
                                                    class="btn btn-xs btn-outline-info">
                                                <i class="fa fa-eye"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- List of Lessons --}}
        <div class="col-lg-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">

                    <h4 class="card-title">@if($moduleSelected) Lecciones del módulo "{{ $moduleSelected->title }}"
                        <button wire:click="addLesson()" class="btn btn-primary pull-right">Crear lección</button> @else
                            Selecciona el módulo  @endif
                    </h4>
                    <div class="list-wrapper">
                        @if ($moduleSelected)
                            <div wire:sortable="sortLessons" id="profile-list-left" class="py-2">
                                @foreach ($moduleSelected->lessons->sortBy('sort') as $lesson)
                                    <div wire:sortable.item="{{ $lesson->id }}" wire:key="lesson-{{ $lesson->id }}"
                                         class="card rounded mb-2">
                                        <div class="card-body p-3">
                                            <div class="media">

                                                <i wire:sortable.handle class="fas fa-grip-vertical fa-2x mr-3"></i>
                                                {{--<img src="../../images/faces/face1.jpg" alt="image"
                                                     class="img-sm mr-3 rounded-circle">--}}
                                                <div class="media-body">

                                                    <h6 class="mb-1">{{ $lesson->title }}</h6>
                                                    {{--<p class="mb-0 text-muted">
                                                        {{ $module->id }} | {{ $module->sort }} Software engineer
                                                    </p>--}}
                                                </div>
                                                {{--<button wire:click="deleteModule({{ $lesson->id }})"
                                                        class="btn btn-xs btn-outline-danger">
                                                    <i class="fa fa-times-circle"></i>
                                                </button>--}}
                                                <button wire:click="selectLesson({{ $lesson->id }})"
                                                        class="btn btn-xs btn-outline-info">
                                                    <i class="fa fa-eye"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                @if ($moduleSelected->evaluation)
                                    <div class="card rounded mb-2">
                                        <div class="card-body p-3">
                                            <div class="media">
                                                <i class="fas fa-grip-vertical fa-2x mr-3"></i>
                                                <div class="media-body">
                                                    <h6 class="mb-1">Evaluación</h6>
                                                </div>
                                                <button
                                                    wire:click="selectEvaluation({{ $moduleSelected->evaluation->id }})"
                                                    class="btn btn-xs btn-outline-primary">
                                                    <i class="fa fa-eye"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="card rounded mb-2">
                                        <div class="card-body p-3">
                                            <div class="media">
                                                <i class="fas fa-grip-vertical fa-2x mr-3"></i>
                                                <div class="media-body">
                                                    <h6 class="mb-1">Crear evaluación</h6>
                                                </div>
                                                <button
                                                    wire:click="addEvaluation()"
                                                    class="btn btn-xs btn-outline-primary">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        {{-- End List of Lessons --}}

        {{-- Lessons --}}
        <div id="lesson_form"
             @if (!$lessonOpen)
                style="display: none"
             @endif
             class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">
                        @if($lessonSelected) Lección {{ $lessonSelected->title }} @else
                            Crear una lección  @endif
                    </h4>

                    @if ($lessonSelected)
                        <form wire:submit.prevent="updateLesson">
                            @else
                                <form wire:submit.prevent="createLesson">
                                    @endif
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label>Título (requerido *):</label>
                                            {!! Form::text('title', old('title'), ['wire:model' => 'lesson_title','class' => 'form-control form-control-sm'.($errors->has('title') ? ' is-invalid': '')]) !!}
                                            @error('title')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                            <hr>
                                            <div wire:ignore class="col-sm-12">
                                                <label for="mytextarea">Descripción:</label>
                                                <label for="lesson_description"></label><textarea
                                                    id="lesson_description" wire:model="lesson_description"
                                                    name="description">
                                                {{ $lesson_description ?? '' }}
                                            </textarea>
                                                @error('title')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                                @enderror
                                            </div>
                                        </div>
                                        {{--<h5>Vídeo/Imagen de la lección (requerido *):</h5>--}}
                                        <div class="col-sm-4">
                                            <hr>
                                            <h5>Archivo principal:</h5>
                                            {!! Form::file('lesson_resource', ['wire:model' => 'lesson_resource','class' => 'form-control form-control-sm'.($errors->has('lesson_resources') ? ' is-invalid': '')]) !!}
                                            @error('lesson_resources')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                            <p wire:loading wire:target="lesson_resource"><i class="fas fa-upload"></i> Cargando archivo...</p>
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        @if ($lessonSelected)
                                                            @if ($lesson_resource)
                                                                <a target="_blank"
                                                                   href="{{ $lesson_resource->temporaryUrl() }}"
                                                                   class="card-link text-success"><i
                                                                        class="fas fa-file"></i>
                                                                    {{ $lesson_resource->getClientOriginalName() }}</a>
                                                            @else
                                                                <a target="_blank"
                                                                   href="{{ asset('storage/'.$lessonSelected->resource_path) }}"
                                                                   class="card-link text-info"><i
                                                                        class="fas fa-file"></i>
                                                                    {{ 'Recurso actual' }}</a>
                                                            @endif
                                                        @else
                                                            @if ($lesson_resource)
                                                                <a target="_blank"
                                                                   href="{{ $lesson_resource->temporaryUrl() }}"
                                                                   class="card-link text-success"><i
                                                                        class="fas fa-file"></i>
                                                                    {{ $lesson_resource->getClientOriginalName() }}</a>
                                                            @endif
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($lesson_resource)
                                                            <a style="cursor: pointer"
                                                               wire:click="deleteLessonResource()"><i
                                                                    class="fas fa-times"></i></a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>

                                        <div class="col-sm-4">
                                            <hr>
                                            <h5>Adjuntos:</h5>
                                            {!! Form::file('resources[]', ['wire:model' => 'resources','multiple','class' => 'form-control form-control-sm'.($errors->has('resources') ? ' is-invalid': '')]) !!}
                                            @error('resources')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                            {{--@if ($resources)--}}
                                            <p wire:loading wire:target="resources"><i class="fas fa-upload"></i> Cargando archivo...</p>
                                            <table class="table">
                                                @if ($lessonSelected)
                                                    @if ($resources)
                                                        @foreach ($resources as $key => $resource)
                                                            <tr>
                                                                <td>
                                                                    <a target="_blank"
                                                                       href="{{ $resource->temporaryUrl() }}"
                                                                       class="card-link text-success"><i
                                                                            class="fas fa-file"></i>
                                                                        {{ $resource->getClientOriginalName() }}
                                                                    </a>
                                                                </td>
                                                                <td>
                                                                    <a style="cursor: pointer"
                                                                       wire:click="deleteLessonFiles({{ $key }}, true)"><i
                                                                            class="fas fa-times"></i></a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif

                                                    @foreach ($lessonSelected->files as $k => $file)
                                                        <tr>
                                                            <td>
                                                                <a target="_blank"
                                                                   href="{{ asset('storage/'.$file->filepath) }}"
                                                                   class="card-link text-info"><i
                                                                        class="fas fa-file"></i>
                                                                    {{ $file->filename  }}
                                                                </a>
                                                            </td>
                                                            <td>
                                                                <a style="cursor: pointer"
                                                                   wire:click="deleteLessonFiles({{ $file->id }})"><i
                                                                        class="fa fa-trash"></i></a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    @if ($resources)
                                                        @foreach ($resources as $key => $resource)
                                                            <tr>
                                                                <td>
                                                                    <a target="_blank"
                                                                       href="{{ $resource->temporaryUrl() }}"
                                                                       class="card-link text-success"><i
                                                                            class="fas fa-file"></i>
                                                                        {{ $resource->getClientOriginalName() }}
                                                                    </a>
                                                                </td>
                                                                <td>
                                                                    <a wire:click="deleteLessonFiles({{ $key }}, true)"
                                                                       href="#"><i class="fa fa-trash"></i></a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                @endif
                                            </table>
                                            {{--@endif--}}
                                        </div>
                                    </div>
                                    <div class="form-group row">

                                    </div>
                                    <button type="submit"
                                            class="btn btn-success">{{ $lessonSelected ? 'Guardar cambios' : 'Agregar lección'}}</button>

                                    @if ($confirmDeleteLesson)
                                        <button type="button" class="btn btn-warning"
                                                wire:click="deleteLesson({{ $lessonSelected->id }})">¿Estás seguro de eliminar?
                                        </button>
                                        <button type="button" class="btn btn-default"
                                                wire:click="$toggle('confirmDeleteLesson')">Cancelar
                                        </button>
                                    @else
                                        @if ($lessonSelected)
                                            <button type="button" class="btn btn-danger"
                                                    wire:click="$toggle('confirmDeleteLesson')">Eliminar
                                            </button>
                                        @endif
                                    @endif
                                </form>
                </div>
            </div>
        </div>
        {{-- End Lesson --}}

        <div
            @if (!$evaluationOpen) style="display: none" @endif id="evaluation_form" class="col-lg-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">
                        Crear una evaluación para el módulo "{{ $moduleSelected->title ?? '' }}"
                    </h4>
                    <div class="add-items d-flex">
                        <div class="col-7">
                            <input type="text" wire:model="evalItemTitle"
                                                  class="form-control form-control-sm is-invalid"
                                                  placeholder="Crea un item o pregunta">
                            @error('evalItemTitle')
                            <span class="invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-2"><input type="text" wire:model="evalItemPoints"
                                                  class="form-control   form-control-sm"
                                                  placeholder="Puntos"></div>
                        <div class="col-3">
                            <button class="btn btn-primary btn-sm font-weight-bold" wire:click="addEvalItem()">Agregar
                                item
                            </button>
                        </div>
                    </div>
                    <hr>
                    <hr>
                    @if ($evalItems)
                        @foreach ($evalItems as $key => $item)
                            {{-- Item de evaluacion--}}
                            <div class="col-12 mt-3 mb-3 ">
                                <div class="card rounded mb-2">
                                    <div class="card-body  m-0 pb-0 pt-0 ">
                                        <div class="row">
                                            <div class="col-sm-10 pt-3 pb-2">
                                                <h5>[{{ $key+1 }}] {{ $item['title'] }}</h5>
                                                @if ($item['points'])
                                                    <p>{{ $item['points'] }} puntos</p>
                                                @endif
                                            </div>
                                            <div class="col-sm-2  pt-2  ">
                                                <button class="btn btn-outline-info btn-xs pull-right"
                                                        wire:click="deleteItem({{ $key }})">Eliminar Item
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="list-wrapper ">
                                    <ul class="d-flex flex-column">
                                        @isset ($evalItems[$key]['options'])
                                            @foreach ($evalItems[$key]['options'] as $k => $opt)
                                                <div class="row">
                                                    <li>
                                                        <div class="col-sm-6">
                                                            <input
                                                                wire:model="evalItems.{{ $key }}.options.{{ $k }}.answer"
                                                                wire:click="uncheckAllItemOptions({{ $key }}, {{ $k }})"
                                                                style="margin-right: 0px" type="checkbox"
                                                                id="itemOption_{{ $k.$key }}"
                                                                name="itemOption_{{ $key }}">
                                                            <label for="itemOption_{{ $k.$key }}"> Es respuesta</label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input
                                                                wire:model="evalItems.{{ $key }}.options.{{ $k }}.title"
                                                                class="form-control form-control-sm" type="text">
                                                        </div>
                                                        <button wire:click="deleteItemOption({{ $key }},{{ $k }})"
                                                                class="btn btn-xs btn-outline-danger">
                                                            <i class=" fa fa-times-circle"></i>
                                                        </button>
                                                    </li>
                                                </div>
                                            @endforeach
                                        @endisset
                                        <li>
                                            <button class="btn btn-outline-info btn-xs"
                                                    wire:click="addItemOption({{ $key }})">Agregar opción
                                            </button>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                            {{-- FIN Item de evaluacion--}}
                        @endforeach
                    @endif
                </div>
                <input type="submit" class="btn btn-success pull-right" wire:click="saveEvaluation"
                       @if (count($evalItems) == 0) disabled @endif>
            </div>
        </div>
    </div>

    @include('administration.courses.partials.modal_form_lesson',['module' => $moduleSelected ?? null])

    @push('livewire_scripts')
        <script>
            /**
             * Tinymce Text Editor
             */
            tinymce.init({
                menubar: false,
                height: 250,
                selector: '#lesson_description',
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table paste code help wordcount'
                ],
                toolbar: 'undo redo | bold italic underline strikethrough ' +
                    '| fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | ' +
                    'outdent indent |  numlist bullist | forecolor backcolor removeformat | emoticons | fullscreen  preview',
                setup: function (editor) {
                    editor.on('init change', function () {
                        editor.save();
                    });
                    editor.on('change', function (e) {
                        @this.
                        set('lesson_description', editor.getContent());
                    });
                },
            });

            /**
             * Tinymce Text Editor
             */
            tinymce.init({
                menubar: false,
                height: 250,
                selector: '#course_description',
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table paste code help wordcount'
                ],
                toolbar: 'undo redo | bold italic underline strikethrough ' +
                    '| fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | ' +
                    'outdent indent |  numlist bullist | forecolor backcolor removeformat | emoticons | fullscreen  preview',
                setup: function (editor) {
                    editor.on('init change', function () {
                        editor.save();
                    });
                    editor.on('change', function (e) {
                        @this.
                        set('lesson_description', editor.getContent());
                    });
                },
            });
        </script>
    @endpush
</div>
