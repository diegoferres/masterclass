<section class="mc-footer-16">
    <!-- /footer-16 -->
    <div class="footer-16-main">
        <div class="wrapper">
            <div class="grid">
                <div class="column">
                    <h3>Masquelier</h3>
                    <ul class="footer-gd-16">
                        <li><a href="https://www.masquelier.com.py">Nosotros</a></li>
                        <li><a href="https://www.masquelier.com.py">Servicios</a></li>
                        <li><a href="https://www.masquelier.com.py">Urgencias</a></li>
                        <li><a href="https://www.masquelier.com.py">Contactos</a></li>
                    </ul>
                </div>
                <div class="column">
                    <h3>Masquelier</h3>
                    <ul class="footer-gd-16">
                        <li><a href="https://www.masquelier.com.py">Nosotros</a></li>
                        <li><a href="https://www.masquelier.com.py">Servicios</a></li>
                        <li><a href="https://www.masquelier.com.py">Urgencias</a></li>
                        <li><a href="https://www.masquelier.com.py">Contactos</a></li>
                    </ul>
                </div>
                <div class="column">
                    <h3>Configuración</h3>
                    <ul class="footer-gd-16">
                        <li><a href="/policies">Políticas de privacidad</a></li>
                        <li><a href="/terms-conditions">Términos y condiciones</a></li>
                    </ul>
                </div>
                <div class="column column3">
                    <h3>Enlaces</h3>
                    <ul class="footer-gd-16">
                        <li><a href="{{ route('profile') }}">Mi cuenta</a></li>
                        <li><a href="{{ route('profile') }}">Lista de cursos</a></li>
                        <li><a href="{{ route('home') }}">Cursos disponibles</a></li>
                        {{--<li><a href="about.html">Faqs</a></li>--}}
                    </ul>
                </div>
                <div class="column column4">
                    <h3>Newsletter </h3>
                    <form action="#" class="subscribe" method="post">
                        <input type="email" name="email" placeholder="Email" required="">
                        <button><span class="fa fa-envelope-o"></span></button>
                    </form>
                    <p class="t-white">Suscríbete y recibe nuestro boletín semanal</p>
                    <p class="t-white">Nunca compartiremos su dirección de correo electrónico</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="mc-footers-1">
    <div class="footer">
        <div class="wrapper">
            <div class="footer-content">
                <div class="d-grid grid-columns-2">
                    <div class="footer-left">
                        <p>© 2021 Masquelier. Todos los derechos reservados</p>
                    </div>
                    <div class="footer-right align-right">
                        <ul class="social">
                            <li><a href="#twitter"><span class="fa fa-facebook"></span></a></li>
                            <li><a href="#facebook"><span class="fa fa-twitter"></span></a></li>
                            <li><a href="#facebook"><span class="fa fa-google-plus"></span></a></li>
                            <li><a href="#facebook"><span class="fa fa-linkedin"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- move top -->
        <button onclick="topFunction()" id="movetop" title="Go to top">
            <span class="fa fa-angle-up"></span>
        </button>
        <!-- /move top -->
    </div>

</section>
