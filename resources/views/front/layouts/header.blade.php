<!-- Headers-4 block -->
<section class="mc-header-4 text-white bg-dark">
    <header id="headers4-block">


        <div class="wrapper">
            <div class="d-grid nav-mobile-block header-align full-width">
                <div class="logo">

                    <a class="brand-logo" href="{{ route('home') }}">
                        <img src="/front/assets/images/masquelier_brand.png" class="img-responsive" alt="Masquelier Medicina Integrativa" title="Masquelier Medicina Integrativa"/>
                    </a>
                </div>
                <input type="checkbox" id="nav">
{{--                <label class="nav" for="nav"></label>--}}
                <nav class="navbar navbar-expand-lg navbar-dark bg-dark  pull-right ">

                    <label for="drop" class="navbar-toggler">
                        <span class="navbar-toggler-icon"></span>
                    </label>


                    <input type="checkbox" id="drop">

                    <ul class="menu">
                        <li><a href="{{ route('home') }}">Cursos</a></li>

                        {{--<li><a href="about.html">Cursos destacados</a></li>--}}

                        <li><a href="https://www.masquelier.com.py/">Contactos</a></li>
                        @guest()
                            <li class="m-2"> <a href="{{ route('login') }}" class="btn btn-outline-primary text-white">Iniciar Sesión</a></li>
                            <li class="m-2"> <a href="{{ route('register') }}" class="btn btn-success text-white">Registrarse</a></li>
                        @else
                            <li>
                                <div class="dropdown">
                                    <a href="" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-user-o"></i> <span>{{ Auth::user()->name }}</span>
                                    </a>
                                    <div class="dropdown-menu" style="background: rgba(0,0,0, 0.5)">
                                        <a class="dropdown-item pl-3" href="{{ route('profile') }}"><span class="text-white">Mis cursos</span> </a>
                                        <a class="dropdown-item pl-3" href="{{ route('profile') }}"><span class="text-white">Mis datos</span></a>

                                        <hr style="border-color: #fff">
                                        <a class="dropdown-item pl-3 text-white" href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                            <span class="text-white">Cerrar sesión</span>
                                            </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </div>
                                </div>
                            </li>
                        @endauth
                    </ul>
                </nav>

            </div>
        </div>
    </header>
</section>

<!-- Headers-4 block -->
