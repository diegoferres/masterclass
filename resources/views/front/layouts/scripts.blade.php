<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
        crossorigin="anonymous"></script>
<script>
    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function () {
        scrollFunction()
    };

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("movetop").style.display = "block";
        } else {
            document.getElementById("movetop").style.display = "none";
        }
    }

    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }
</script>
<script src='{{asset('front/assets/js/jquery-3.6.0.min.js')}}'></script>
<script src='{{asset('front/assets/js/jquery-ui.min.js')}}'></script>
<script>
    $('#drop').change(function () {
        if ($('#drop').is(":checked")) {
            $('body').css('overflow', 'hidden');
        } else {
            $('body').css('overflow', 'auto');
        }
    });


</script>
@livewireStyles
@stack('livewire_scripts')
@yield('scripts')
<script>
    //Filter Div
    $(document).ready(function () {
        $(".filter-button").click(function () {
            var value = $(this).attr('data-filter');
            if (value == "all") {
                //$('.filter').removeClass('hidden').animate({width: 100, opacity: 1}, 'fast').fadeIn();
                $('.filter').removeClass('hidden').animate({width: 360, opacity: 1}, 'fast').fadeIn();
            } else {
                //$(".filter").not('.' + value).animate({width: 0, opacity: 0}, 'fast').fadeOut();
                $(".filter").not('.' + value).animate({width: 0, opacity: 0}, 'fast').fadeOut();
                //$('.filter').filter('.' + value).animate({width: 100, opacity: 1}, 'fast').fadeIn();
                $('.filter').filter('.' + value).animate({width: 360, opacity: 1}, 'fast').fadeIn();
            }
        });
    });
</script>
