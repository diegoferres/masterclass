<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Masquelier | Medicina Integrativa</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
      integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

<link rel="stylesheet" href="/front/assets/css/style.css">
<link rel="stylesheet" href="/front/assets/css/style2.css">

<!--link rel="stylesheet" href="/assests/css/font-awesome.min.css"-->
{{--<script src='/front/assets/1.10.2/jquery.min.js'></script>--}}

<style>
    /*Filter Div*/
    /*-----------------------------------------------------------*/
    .port-image { width: 100%; }
    .col-md-3 { margin-bottom:20px; }
    .each-item { position:relative;  overflow:hidden; }
    .each-item:hover .cap2, .each-item:hover .cap1 { left:0px; }
    .cap1
    {
        position:absolute;
        width:100%;
        height:70%;
        background:rgba(255, 255, 255, 0.5);
        top:0px;
        left:-100%;
        padding:10px;
        transition: all .5s;
    }
    .cap2
    {
        position:absolute;
        width:100%;
        height:30%;
        background:rgba(0, 178, 255, 0.5);
        bottom:0px;
        left:100%;
        padding:10px;
        transition: all .5s;
    }
    /*Fin Filter Div*/
    /*-----------------------------------------------------------*/

    /*
        Header dropdown
     */
    .dropdown-item:hover, .dropdown-item:focus {
        color: #16181b;
        text-decoration: none;
        background-color: #000;
    }

    .dropdown-item.active, .dropdown-item:active {
        color: #fff;
        text-decoration: none;
        background-color: #000;
    }
</style>

@livewireStyles
@yield('styles')
