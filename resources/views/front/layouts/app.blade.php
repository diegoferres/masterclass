<html lang="es">
<head>
    @include('front.layouts.head')
</head>
<body>

@section('header')
    @include('front.layouts.header')
@show


@yield('content')

@include('front.layouts.footer')

@include('front.layouts.scripts')
