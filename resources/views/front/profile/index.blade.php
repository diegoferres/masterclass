﻿@extends('front.layouts.app')
@section('style')
    <style>
        .cropit-preview {
            background-color: #f8f8f8;
            background-size: cover;
            border: 5px solid #ccc;
            border-radius: 3px;
            margin-top: 7px;
            width: 250px;
            height: 250px;
        }

        .cropit-preview-image-container {
            cursor: move;
        }

        .cropit-preview-background {
            opacity: .2;
            cursor: auto;
        }

        .image-size-label {
            margin-top: 10px;
        }

        input, .export {
            /* Use relative position to prevent from being covered by image background */
            position: relative;
            z-index: 10;
            display: block;
        }

        button {
            margin-top: 10px;
        }

        .btn-delete-img {
            background-color: #c13b2a;
            color: #fff;
            font-size: 1.5rem;
            padding: 5px 14px;
            width: 50px;
            height: 50px;
            position: relative;
            top: -330px;
            left: 280px;
        }
    </style>
@endsection('style')
@section('content')

    <!---728x90--->
    <section style="background: #e9eaea">
        <div class="container ">
            <div class="row  ">
                <div class="col-md-4 mt-3 mb-3">
                    <div class="card   ">
                        <div class="card-body">


                            <div class="text-center">
                                <img class="[ rounded-circle ]" style="border: 3px solid #589bd6"
                                     src="https://ui-avatars.com/api/?name={{ Auth::user()->name }}"
                                     class="avatar img-circle img-thumbnail" alt="avatar" width="150vh">
                                <div class="panel panel-default">
                                    <div class="panel-heading">{{ Auth::user()->name }}</div>
                                </div>
                            </div>
                            </hr><br>


                            <ul class="list-group">
                                <li class="list-group-item text-muted">Mis cursos <i class="fa fa-dashboard fa-1x"></i>
                                </li>
                                @foreach (\Auth::user()->courses as $course)
                                    <li class="list-group-item text-right">
                                        <a href="{{ route('course.show', $course->slug) }}">
                                            <span class="pull-left"><strong>{{ $course->title }}</strong></span>
                                        </a>
                                    </li>
                                @endforeach
                                {{--<li class="list-group-item text-right"><span class="pull-left"><strong>Venas y arterias 2</strong></span>
                                    10%
                                </li>
                                <li class="list-group-item text-right"><span
                                        class="pull-left"><strong>Virología 1</strong></span> 85%
                                </li>
                                <li class="list-group-item text-right"><span class="pull-left"><strong>Medicina laboral 3</strong></span>
                                    100%
                                </li>--}}
                            </ul>


                            {{--<div class="card-footer text-center">

                                <button class="  btn btn-success"><i class="fa fa-book"></i> Todos mis cursos</button>

                            </div>--}}
                        </div>
                    </div>
                </div>
                <div class="col-md-8 mt-3 mb-3">
                    <div class="card">
                        <div class="card-body">

                            <section id="tabs">
                                <div class="container  ">
                                    <div class="row">
                                        <div class="col-sm-12 ">
                                            <nav>
                                                <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">

                                                    <a class="nav-item nav-link active  " id="nav-perfil-tab"
                                                       data-toggle="tab"
                                                       href="#nav-perfil" role="tab" aria-controls="nav-perfil"
                                                       aria-selected="false">Datos personales</a>
                                                    <a class="nav-item nav-link   " id="nav-facturacion-tab"
                                                       data-toggle="tab"
                                                       href="#nav-facturacion" role="tab"
                                                       aria-controls="nav-facturacion"
                                                       aria-selected="false">Facturación</a>
                                                </div>
                                            </nav>
                                            <br>
                                            <div class="tab-content " id="nav-tabContent">

                                                <div class="tab-pane fade show active" id="nav-perfil" role="tabpanel"
                                                     aria-labelledby="nav-perfil-tab">


                                                    <div class="mc-module-main">
                                                        <div class="module-sec">
                                                            <div class="wrapper">
                                                                <!-- module -->
                                                                <div class="d-grid module-cont">
                                                                    <div class="about-right-module">


                                                                        <!-- //module -->
                                                                        <!-- accordions -->
                                                                        <div class="sub-accor">
                                                                            {!! Form::open(['route' => ['profile.update', Auth::id()], 'method' => 'post']) !!}


                                                                                <div class="form-group">

                                                                                    <div class="col-xs-6">
                                                                                        <label for="first_name"><h4>
                                                                                                Nombres y Apellidos</h4>
                                                                                        </label>
                                                                                        <input type="text"
                                                                                               class="form-control @error('name') is-invalid @enderror"
                                                                                               name="name"
                                                                                               id="first_name"
                                                                                               placeholder="first name"
                                                                                               title="enter your first name if any."
                                                                                               value="{{ Auth::user()->name }}">
                                                                                        @error('password')
                                                                                        <span class="invalid-feedback">{{ $message }}</span>
                                                                                        @enderror
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <div class="col-xs-6">
                                                                                        <label for="email"><h4>
                                                                                                Email</h4></label>
                                                                                        <input disabled type="email"
                                                                                               class="form-control"
                                                                                               name="email" id="email"
                                                                                               value="{{ Auth::user()->email }}"
                                                                                               title="Ingrese su correo">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">

                                                                                    <div class="col-xs-6">
                                                                                        <label for="password"><h4>
                                                                                                Contraseña nueva</h4>
                                                                                        </label>
                                                                                        <input type="password"
                                                                                               class="form-control @error('password') is-invalid @enderror"
                                                                                               name="password"
                                                                                               id="password"
                                                                                               placeholder="Incluya mayúsculas, minúsculas y números"
                                                                                               title="Ingrese su contraseña">
                                                                                        @error('password')
                                                                                        <span class="invalid-feedback">{{ $message }}</span>
                                                                                        @enderror
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">

                                                                                    <div class="col-xs-6">
                                                                                        <label for="password2"><h4>
                                                                                                Repetir contraseña</h4>
                                                                                        </label>
                                                                                        <input type="password"
                                                                                               class="form-control"
                                                                                               name="password_confirmation"
                                                                                               id="password2"
                                                                                               title="Repita su contraseña">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <div class="col-xs-12">
                                                                                        <br>
                                                                                        <button
                                                                                            class="btn btn-lg btn-success"
                                                                                            type="submit"><i
                                                                                                class="fa fa-save"></i>
                                                                                            Guardar
                                                                                        </button>
                                                                                    </div>
                                                                                </div>
                                                                            {!! Form::close() !!}
                                                                        </div>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <hr>
                                                                        <br>
                                                                    </div>


                                                                </div>
                                                                <!---728x90--->


                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="tab-pane fade" id="nav-facturacion" role="tabpanel"
                                                     aria-labelledby="nav-facturacion-tab">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <th>ID</th>
                                                            <th>Curso</th>
                                                            <th>Monto</th>
                                                            <th>Fecha</th>
                                                        </thead>
                                                        <tbody>
                                                        @foreach (\Auth::user()->orders as $order)
                                                            <tr>
                                                                <td>{{ $order->id }}</td>
                                                                <td>{{ $order->course->title }}</td>
                                                                <td>{{ number_format($order->amount, 0, ',', '.') }}</td>
                                                                <td>{{ $order->created_at }}</td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </section>

                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>
    <!---728x90--->

@endsection
@section('scripts')

@endsection
