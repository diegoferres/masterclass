@extends('front.layouts.app')
@section('content')
<!-- inner banner -->
<section class="inner-banner-main">
<div class="about-inner">
<div class="wrapper">

<ul class="breadcrumbs-custom-path">
<h3>Ecommerce</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
</ul>
</div>
</div>
</section>
<!---728x90--->
<!---728x90--->
<section class="" style="background: #e9eaea">
<div class="container ">
<div class="row  ">

<div class="col-md-12  mt-3 mb-3 ">
<div class="card">
<div class="card-body">
<section id="tabs">
<div class="container  ">
<div class="row">
<div class="col-sm-12 ">
<nav>
<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
<a class="nav-item nav-link active  " id="nav-contenido-tab"
data-toggle="tab"
href="#nav-contenido" role="tab" aria-controls="nav-home"
aria-selected="true">Todos</a>
<a class="nav-item nav-link  " id="nav-descripcion-tab"
data-toggle="tab"
href="#nav-descripcion" role="tab" aria-controls="nav-profile"
aria-selected="false">Activos</a>
<a class="nav-item nav-link   " id="nav-comentarios-tab"
data-toggle="tab"
href="#nav-comentarios" role="tab" aria-controls="nav-contact"
aria-selected="false">Superados</a>
</div>
</nav>
<br>

<div class="tab-content " id="nav-tabContent">
<div class="tab-pane fade show active" id="nav-contenido"
role="tabpanel"
aria-labelledby="nav-contenido-tab">

<div class="mc-module-main">
<div class="module-sec">
<div class="wrapper">
<!-- module -->
<div class="d-grid module-cont">
<div class="about-right-module">

<!-- //module -->
<!-- accordions -->
<div class="row">
<div class="col-sm-8">
<div class="sub-accor">
<ul class="accordion css-accordion">
<li class="accordion-item">
<input class="accordion-item-input"
type="checkbox"
name="accordion" id="item2">

<label for="item2"
class="accordion-item-hd">

<div class="row">

<div class="col-sm-6">
<h3><a href=""
aria-current="true">Presentación
del curso</a></h3>
<br>

<div class="text-left" style="position: absolute;">
    <span>25%</span>
</div>
<div class="text-right">
    <span>2/10</span>
</div>
<div class="progress"
style="height: 9px;">
<div
class="progress-bar"
role="progressbar"
style="width: 25%;"
aria-valuenow="25"
aria-valuemin="0"
aria-valuemax="100"></div>
</div>
<span class="text-gray">Faltan 34 días para terminar  <i class="fa fa-clock-o"></i></span>
<br> 
<br>
<div> 
    <a href="" class="btn btn-success"> Certificado</a>
    <a href="" class="btn btn-info"> Modulos</a>
</div>

</div>


</div>

</label>
<div
class="accordion-item-bd accordion-item-bd-2">
<div class="form-check">
<div class="row">
<div class="col-sm-6">
<div class="checkbox">
<label>
<input type="checkbox" value="">
<span class="cr"><i class="cr-icon fa fa-check"></i></span>
<a href="{{ route('course.lesson') }}">Nombre de
la clase </a>
</label>
</div>
</div>
<div class="col-sm-6">
<h6 class="text-right">
<a
href="modulos.html"><i
class=" fa fa-play"></i>
08:30
min</a></h6>
</div>
</div>


</div>
<hr>
<div class="form-check">
<div class="row">
<div class="col-sm-6">
<div class="checkbox">
<label>
<input type="checkbox" value="">
<span class="cr"><i class="cr-icon fa fa-check"></i></span>
<a href="{{ route('course.lesson') }}">Nombre de
la clase </a>
</label>
</div>
</div>
<div class="col-sm-6">
<h6 class="text-right">
<i
class=" fa fa-play"></i>
08:30
min
</h6>
</div>
</div>


</div>
<hr>
<div class="form-check">
<div class="row">
<div class="col-sm-6">
<div class="checkbox">
<label>
<input type="checkbox" value="">
<span class="cr"><i class="cr-icon fa fa-check"></i></span>
<a href="{{ route('course.lesson') }}">Nombre de
la clase </a>
</label>
</div>
</div>
<div class="col-sm-6">
<h6 class="text-right">
<i
class=" fa fa-play"></i>
08:30
min
</h6>
</div>
</div>


</div>
</div>

</li>


</ul>
<!-- //accordions -->
</div>
</div>
</div>

</div>


</div>
<!---728x90--->


</div>
</div>
</div>
<br>
<hr>
<br>
</div>
<div class="tab-pane fade" id="nav-descripcion" role="tabpanel"
aria-labelledby="nav-descripcion-tab">


<div class="mc-module-main">
<div class="module-sec">
<div class="wrapper">
<!-- module -->
<div class="d-grid module-cont">
<div class="about-right-module">


<!-- //module -->
<!-- accordions -->
<div class="sub-accor">
<h3 class="title-w3-ab text-uppercase mt-5 mb-4">
Lección
1.</h3>
<p>detalles del curso en cuestion</p>
<!-- //accordions -->
</div>
<br>
<br>
<br>
<hr>
<br>
</div>


</div>
<!---728x90--->


</div>
</div>
</div>


</div>
<div class="tab-pane fade" id="nav-comentarios" role="tabpanel"
aria-labelledby="nav-comentarios-tab">
<div class="card">
<div class="[ panel panel-default ] panel-google-plus">


<div class="panel-heading">
<img class="[ rounded-circle pull-left ]"
src="https://lh3.googleusercontent.com/-CxXg7_7ylq4/AAAAAAAAAAI/AAAAAAAAAQ8/LhCIKQC5Aq4/s46-c-k-no/photo.jpg"
alt="Mouse0270"/>
<h3>Diego Ferreira</h3>
<h5><span>comentado el</span>
<span>27, marzo 2021</span></h5>
</div>
<div class="panel-body">
<p>Hola Doc!. ¿Cómo justifica el aumento de globulos
rojos?</p>
</div>
</div>
<div
class="[ panel panel-default ] panel-google-plus comments-respuesta">
<div class="panel-heading comments-doc">
<img class="[ rounded-circle pull-left ]"
src="https://lh3.googleusercontent.com/-CxXg7_7ylq4/AAAAAAAAAAI/AAAAAAAAAQ8/LhCIKQC5Aq4/s46-c-k-no/photo.jpg"
alt="Mouse0270"/>
<h3>Doc. Martinez</h3>
<h5><span>comentado el</span>
<span>27, marzo 2021</span></h5>

</div>
<div class="panel-body">
<p>Hola Diego, el aumento de globulos rojos se debe al
alto grado de
exposicion
.</p>
</div>
</div>
<div class="[ panel panel-default ] panel-google-plus">
<div class="panel-heading">
<img class="[ rounded-circle pull-left ]"
src="https://lh3.googleusercontent.com/-CxXg7_7ylq4/AAAAAAAAAAI/AAAAAAAAAQ8/LhCIKQC5Aq4/s46-c-k-no/photo.jpg"
alt="Mouse0270"/>
<h3>Diego Ferreira</h3>
<h5><span>comentado el</span>
<span>27, marzo 2021</span></h5>
</div>
<div class="panel-body">
<p>Tienen tiempo de presentar el trabajo hasta las 23:00
de dia 3 de
abril
del
lectivo año.</p>
</div>


<div class="panel-footer">
<div class="input-placeholder">Dejar comentario...</div>
</div>
<div class="panel-google-plus-comment">
<img class="rounded-circle"
src="https://lh3.googleusercontent.com/uFp_tsTJboUY7kue5XAsGA=s46"
alt="User Image"/>
<div class="panel-google-plus-textarea">
<textarea rows="4"></textarea>
<button type="submit"
class="[ btn btn-success disabled ]">
Comentar
</button>
<button type="reset" class="[ btn btn-default ]">
Cancel
</button>
</div>
<div class="clearfix"></div>
</div>


</div>
</div>
</div>

</div>

</div>
</div>
</div>
</section>
</div>
</div>
</div>
</div>
</div>
</section>
<!---728x90--->
@endsection
@section('scripts')
<script>
$(function () {
$('.panel-google-plus > .panel-footer > .input-placeholder, .panel-google-plus > .panel-google-plus-comment > .panel-google-plus-textarea > button[type="reset"]').on('click', function (event) {
var $panel = $(this).closest('.panel-google-plus');
$comment = $panel.find('.panel-google-plus-comment');

$comment.find('.btn:first-child').addClass('disabled');
$comment.find('textarea').val('');

$panel.toggleClass('panel-google-plus-show-comment');

if ($panel.hasClass('panel-google-plus-show-comment')) {
$comment.find('textarea').focus();
}
});
$('.panel-google-plus-comment > .panel-google-plus-textarea > textarea').on('keyup', function (event) {
var $comment = $(this).closest('.panel-google-plus-comment');

$comment.find('button[type="submit"]').addClass('disabled');
if ($(this).val().length >= 1) {
$comment.find('button[type="submit"]').removeClass('disabled');
}
});
});
</script>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
crossorigin="anonymous"></script>
@endsection
