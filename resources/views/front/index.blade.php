﻿@extends('front.layouts.app')
@section('styles')
    <style>
        .search input {
            width: 90%;
            padding: 15px 60px;
            border: 0;
            box-shadow: 0px 2px 6px #00000024;
            border-radius: 12px;
            z-index: 300;
        }

        .search .icon {
            position: relative;
            height: 20px;
            width: 20px;
            left: 45px;
            font-size: 16px;
            color: #7E7D83;
            /*top: 14px;*/
        }

        input:focus {
            outline: none;
        }
    </style>
@endsection
@section('content')
    <div class="mc-products-1">
        <div id="products1-block">
            <div class="wrapper">
                <div class="row justify-content-center mt-5">
                    <div class="col-md-7 text-center mt-4">
                        <h2>Encuentra tu curso</h2>
                        <form action="{{ route('home') }}" method="GET" class="typeahead" role="search">
                            <div class="search pt-3 mb-5">
                                <i class="fa fa-search icon" style="z-index: 2000;"></i>
                                <input style="border-color: transparent" type="search" name="searchTerm" autocomplete="off"
                                       placeholder="Encuentra el tuyo entre todos los cursos"
                                       aria-label="Encuentra el tuyo entre todos los cursos" aria-describedby="btnsearch"
                                       class="search-input" value="{{ $_GET['searchTerm'] ?? null }}">
                            </div>
                        </form>
                    </div>
                </div>

                <div class="mt-3">

                    <button class="btn btn-success filter-button" data-filter="all">Todo</button>
                    @foreach ($categories as $category)
                        <button class="btn btn-info filter-button"
                                data-filter="{{ str_replace(' ', '',$category->description) }}">{{ $category->description }}</button>
                    @endforeach
                </div>
                <br/>

                <div class="row ">

                    <div class="d-grid grid-col-3">
                        @foreach ($courses as $course)
                            <div
                                class="product filter @foreach($course->categories as $category){{ $category->description }} @endforeach">
                                <a href="{{ route('course.show', $course->slug) }}"><img
                                        src="{{ asset('storage/'.$course->intro_resource) }}"
                                        class="img-responsive"
                                        alt=""></a>
                                <div class="info-bg">
                                    <h5><a href="{{ route('course.show', $course->slug) }}">{{ $course->title }}</a>
                                    </h5>
                                    {{--<ul class="star">
                                        <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                        <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                        <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                        <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                        <li><span class="fa fa-star-o" aria-hidden="true"></span></li>
                                    </ul>--}}
                                    <ul class="d-flex">
                                        <li class="t-gray"><span class="fa fa-clock-o"></span><span class="t-15"> Creado el: {{ date('d/m/Y', strtotime($course->created_at)) }}</span>
                                        </li>
                                        <li class="margin-effe"><a href="{{ route('course.show', $course->slug) }}" title="Add this to Favorite"><span
                                                    class="fa fa-book color-green"></span></a></li>
                                    </ul>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!---728x90--->
@endsection
