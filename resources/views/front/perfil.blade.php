@extends('front.layouts.app')
{{--@section('header')
    @parent
    <button>AAAAAAA</button>
@stop--}}
@section('content')
    <!-- inner banner -->
    <section class="inner-banner-main">
        <div class="about-inner">
            <div class="wrapper">

                <ul class="breadcrumbs-custom-path">
                    <h3>Perfil</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                </ul>
            </div>
        </div>
    </section>
    <!---728x90--->

    <!-- //covers -->
    <div class="mc-products-1">
        <div id="products1-block" >
            <div class="wrapper">
                <h1 class="mb-5 m-3">¿Porqué elegir nuestros cursos? </h1>
                <div class="row mb-5 text-center">
                    <div class="  col-md-4 ">
                        <div class="form-group m-4">
                            <i class="bi-award  " style="font-size: 4rem; color: #95bc58;"></i>
                        </div>
                        <h2>Primer Nivel</h2>
                        <p class="text-justify">Paragraph of text beneath the heading to explain the heading. We'll add onto it with another
                            sentence and probably just keep going .</p>
                        <div class="form-group mt-3">
                            <a href="#" class="btn btn-info ">
                                Call to action
                            </a>
                        </div>
                    </div>
                    <div class="  col-md-4 ">
                        <div class="form-group m-4">
                            <i class="bi-bag-plus  " style="font-size: 4rem; color: #95bc58;"></i>
                        </div>
                        <h2>Mas beneficios</h2>
                        <p class="text-justify">Paragraph of text beneath the heading to explain the heading. We'll add onto it with another
                            sentence and probably just keep going .</p>
                        <div class="form-group mt-3">
                            <a href="#" class="btn btn-info">
                                Call to action
                            </a>
                        </div>
                    </div>
                    <div class="  col-md-4 ">
                        <div class="form-group m-4">
                            <i class="bi-chat-square-dots  " style="font-size: 4rem; color: #95bc58;"></i>
                        </div>
                        <h2>Comunidad</h2>
                        <p class="text-justify">Paragraph of text beneath the heading to explain the heading. We'll add onto it with another
                            sentence and probably just keep going .</p>
                        <div class="form-group mt-3">
                            <a href="#" class="btn btn-info">
                                Call to action
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row  mb-5">
                    <div class="col-lg-6">
                        <h2>Modern Business Features</h2>
                        <p class="text-justify">The Modern Business template by Start Bootstrap includes:</p>


                        <p class="text-justify">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis,
                            omnis doloremque non cum id reprehenderit, quisquam totam aspernatur
                            tempora minima unde aliquid ea culpa sunt. Reiciendis quia dolorum ducimus unde.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis,
                            omnis doloremque non cum id reprehenderit, quisquam totam aspernatur. </p>
                        <p class="text-justify">
                            Tempora minima unde aliquid ea culpa sunt. Reiciendis quia dolorum ducimus unde.</p>
                    </div>
                    <div class="col-lg-6">
                        <img class="img-fluid rounded" src="http://placehold.it/700x450" alt="">
                    </div>
                </div>
                <div class="row ">

                    <div class="d-grid grid-col-3">

                        <div class="product">
                            <a href="details_course.html"><img src="/front/assets\images\course1.png"
                                                               class="img-responsive"
                                                               alt=""></a>
                            <div class="info-bg">
                                <h5><a href="details_course.html">Fortalecimiento efectivo y saludable de la
                                        musculatura.</a></h5>
                                <ul class="star">
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star-o" aria-hidden="true"></span></li>
                                </ul>
                                <ul class="d-flex">
                                    <li class="t-gray"><span class="fa fa-clock-o"></span><span class="t-15"> Inicio: 18 marzo 2021</span>
                                    </li>
                                    <li class="margin-effe"><a href="#fav" title="Add this to Favorite"><span
                                                class="fa fa-book color-green"></span></a></li>

                                </ul>
                            </div>
                        </div>
                        <div class="product">
                            <a href="details_course.html"><img src="/front/assets\images\course2.png"
                                                               class="img-responsive"
                                                               alt=""></a>
                            <div class="info-bg">
                                <h5><a href="details_course.html">Estrategias de Prevención ante Infecciones
                                        Respiratorias
                                        Agudas.</a></h5>
                                <ul class="star">
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star-o" aria-hidden="true"></span></li>
                                </ul>
                                <ul class="d-flex">
                                    <li class="t-gray"><span class="fa fa-clock-o"></span><span class="t-15"> Inicio: 18 marzo 2021</span>
                                    </li>
                                    <li class="margin-effe"><a href="#fav" title="Add this to Favorite"><span
                                                class="fa fa-book color-green"></span></a></li>

                                </ul>
                            </div>
                        </div>
                        <div class="product">
                            <a href="details_course.html"><img src="/front/assets\images\course3.png"
                                                               class="img-responsive"
                                                               alt=""></a>
                            <div class="info-bg">
                                <h5><a href="details_course.html">ÉTICA EN SALUD PÚBLICA. ¿La salud es siempre lo
                                        primero?</a></h5>
                                <ul class="star">
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star-o" aria-hidden="true"></span></li>
                                </ul>
                                <ul class="d-flex">
                                    <li class="t-gray"><span class="fa fa-clock-o"></span><span class="t-15"> Inicio: 18 marzo 2021</span>
                                    </li>
                                    <li class="margin-effe"><a href="#fav" title="Add this to Favorite"><span
                                                class="fa fa-book color-green"></span></a></li>

                                </ul>
                            </div>
                        </div>

                        <!---728x90--->
                        <div class="product">
                            <a href="details_course.html"><img src="/front/assets\images\course1.png"
                                                               class="img-responsive"
                                                               alt=""></a>
                            <div class="info-bg">
                                <h5><a href="details_course.html">Fortalecimiento efectivo y saludable de la
                                        musculatura.</a></h5>
                                <ul class="star">
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star-o" aria-hidden="true"></span></li>
                                </ul>
                                <ul class="d-flex">
                                    <li class="t-gray"><span class="fa fa-clock-o"></span><span class="t-15"> Inicio: 18 marzo 2021</span>
                                    </li>
                                    <li class="margin-effe"><a href="#fav" title="Add this to Favorite"><span
                                                class="fa fa-book color-green"></span></a></li>

                                </ul>
                            </div>
                        </div>
                        <div class="product">
                            <a href="details_course.html"><img src="/front/assets\images\course2.png"
                                                               class="img-responsive"
                                                               alt=""></a>
                            <div class="info-bg">
                                <h5><a href="details_course.html">Estrategias de Prevención ante Infecciones
                                        Respiratorias
                                        Agudas.</a></h5>
                                <ul class="star">
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star-o" aria-hidden="true"></span></li>
                                </ul>
                                <ul class="d-flex">
                                    <li class="t-gray"><span class="fa fa-clock-o"></span><span class="t-15"> Inicio: 18 marzo 2021</span>
                                    </li>
                                    <li class="margin-effe"><a href="#fav" title="Add this to Favorite"><span
                                                class="fa fa-book color-green"></span></a></li>

                                </ul>
                            </div>
                        </div>
                        <div class="product">
                            <a href="details_course.html"><img src="/front/assets\images\course3.png"
                                                               class="img-responsive"
                                                               alt=""></a>
                            <div class="info-bg">
                                <h5><a href="details_course.html">ÉTICA EN SALUD PÚBLICA. ¿La salud es siempre lo
                                        primero?</a></h5>
                                <ul class="star">
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star-o" aria-hidden="true"></span></li>
                                </ul>
                                <ul class="d-flex">
                                    <li class="t-gray"><span class="fa fa-clock-o"></span><span class="t-15"> Inicio: 18 marzo 2021</span>
                                    </li>
                                    <li class="margin-effe"><a href="#fav" title="Add this to Favorite"><span
                                                class="fa fa-book color-green"></span></a></li>

                                </ul>
                            </div>
                        </div>
                        <div class="product">
                            <a href="details_course.html"><img src="/front/assets\images\course1.png"
                                                               class="img-responsive"
                                                               alt=""></a>
                            <div class="info-bg">
                                <h5><a href="details_course.html">Fortalecimiento efectivo y saludable de la
                                        musculatura.</a></h5>
                                <ul class="star">
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star-o" aria-hidden="true"></span></li>
                                </ul>
                                <ul class="d-flex">
                                    <li class="t-gray"><span class="fa fa-clock-o"></span><span class="t-15"> Inicio: 18 marzo 2021</span>
                                    </li>
                                    <li class="margin-effe"><a href="#fav" title="Add this to Favorite"><span
                                                class="fa fa-book color-green"></span></a></li>

                                </ul>
                            </div>
                        </div>
                        <div class="product">
                            <a href="details_course.html"><img src="/front/assets\images\course2.png"
                                                               class="img-responsive"
                                                               alt=""></a>
                            <div class="info-bg">
                                <h5><a href="details_course.html">Estrategias de Prevención ante Infecciones
                                        Respiratorias
                                        Agudas.</a></h5>
                                <ul class="star">
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star-o" aria-hidden="true"></span></li>
                                </ul>
                                <ul class="d-flex">
                                    <li class="t-gray"><span class="fa fa-clock-o"></span><span class="t-15"> Inicio: 18 marzo 2021</span>
                                    </li>
                                    <li class="margin-effe"><a href="#fav" title="Add this to Favorite"><span
                                                class="fa fa-book color-green"></span></a></li>

                                </ul>
                            </div>
                        </div>
                        <div class="product">
                            <a href="details_course.html"><img src="/front/assets\images\course3.png"
                                                               class="img-responsive"
                                                               alt=""></a>
                            <div class="info-bg">
                                <h5><a href="details_course.html">ÉTICA EN SALUD PÚBLICA. ¿La salud es siempre lo
                                        primero?</a></h5>
                                <ul class="star">
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star" aria-hidden="true"></span></li>
                                    <li><span class="fa fa-star-o" aria-hidden="true"></span></li>
                                </ul>
                                <ul class="d-flex">
                                    <li class="t-gray"><span class="fa fa-clock-o"></span><span class="t-15"> Inicio: 18 marzo 2021</span>
                                    </li>
                                    <li class="margin-effe"><a href="#fav" title="Add this to Favorite"><span
                                                class="fa fa-book color-green"></span></a></li>

                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!---728x90--->
@endsection
