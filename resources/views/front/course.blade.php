﻿@extends('front.layouts.app')
@section('content')
    <!-- mc-features-photo-7 -->

    <section class="mc-features-photo-7">
        <div class="mc-features-photo-7_sur">
            <div class="wrapper">
                <div class="mc-features-photo-7_top">
                    <div class="mc-features-photo-7_top-right">
                        <div class="galleryContainer">
                            <div class="gallery">
                                <input type="radio" name="controls" id="c1" checked=""><img class="galleryImage" id="i1"
                                                                                            src="{{ asset('storage/'.$course->intro_resource) }}"
                                                                                            class="img img-responsive"
                                                                                            alt="">
                                <input type="radio" name="controls" id="c2"><img class="galleryImage" id="i2"
                                                                                 src="/front/assets\images\p2.jpg"
                                                                                 class="img img-responsive" alt="">
                                <input type="radio" name="controls" id="c3"><img class="galleryImage" id="i3"
                                                                                 src="/front/assets\images\p3.jpg"
                                                                                 class="img img-responsive" alt="">
                                <input type="radio" name="controls" id="c4"><img class="galleryImage" id="i4"
                                                                                 src="/front/assets\images\p4.jpg"
                                                                                 class="img img-responsive" alt="">
                            </div>
                            {{--<div class="thumbnails">
                                <label class="thumbnailImage " for="c1"><img src="/front/assets\images\course2.png"
                                                                             class="img img-responsive" alt=""></label>
                                <label class="thumbnailImage" for="c2"><img src="/front/assets\images\p2.jpg"
                                                                            class="img img-responsive" alt=""></label>
                                <label class="thumbnailImage" for="c3"><img src="/front/assets\images\p3.jpg"
                                                                            class="img img-responsive" alt=""></label>
                                <label class="thumbnailImage" for="c4"><img src="/front/assets\images\p4.jpg"
                                                                            class="img img-responsive" alt=""></label>
                            </div>--}}
                        </div>
                    </div>
                    <div class="mc-features-photo-7_top-left">
                        <h4>{{ $course->title }}</h4>

                        <p class="coasts">
                            <span class="item_price">Gs. {{ number_format($course->price, 0, '','.') }}</span>
                            {{--<del class="mx-2 font-weight-light">Gs.790.000</del>--}}

                        </p>
                        <p class="para">{!! $course->description !!}</p>
                        <br>


                        <div class="container">
                            <div class="row">
                                @if ($course->datetime_start)
                                    <div class="col-4">
                                        <div class="card">
                                            <div class="card-body">
                                                <i class="fa fa-calendar-check-o color-green" aria-hidden="true"></i>
                                                Inicia: {{ $course->datetime_start }}
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                {{--@if ($course->datetime_end)
                                    <div class="col-4">
                                        <div class="card">
                                            <div class="card-body">
                                                <i class="fa fa-calendar color-green" aria-hidden="true"></i>
                                                Duración: 12 semanas.
                                            </div>
                                        </div>
                                    </div>
                                @endif--}}
                                <div class="col-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <i class="fa fa-bookmark-o color-green" aria-hidden="true"></i>
                                            {{ $course->modules->count() }} módulos
                                        </div>
                                    </div>

                                </div>
                                <div class="col-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <i class="fa fa-bookmark-o color-green" aria-hidden="true"></i>
                                            {{ $course->lessons->count() }} lecciones
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="container">
                            <div class="row">
                                @if ($course->lessons()->orderBy('sort')->first())
                                    @if (isset(Auth::user()->orders) && Auth::user()->orders->where('course_id', $course->id)->first())
                                        <div class="col-sm-12  ">
                                            <a href="{{ route('course.lesson', [$course->slug, $course->lessons()->orderBy('sort')->first()->slug]) }}"
                                               class="btn btn-success t-white btn-block btn-lg ">
                                                {{--<i class="fa fa-calendar-plus-o"></i>--}} Ir a la lección</a>
                                        </div>
                                    @else
                                        <div class="col-sm-12  ">
                                            <a href="{{ route('order.checkout', $course->slug) }}"
                                               class="btn btn-success t-white btn-block btn-lg ">
                                                <i class="fa fa-calendar-plus-o"></i> Inscribirme</a>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="des-bottom">
                    <div class="mc-module-main">
                        <div class="module-sec  ">
                            <div class="wrapper">
                                <!-- module -->
                                <div class="d-grid module-cont">
                                    <div class="about-right-module collapse show">
                                        @foreach ($course->categories as $category)
                                            <div class="btn btn-outline-primary">
                                                <i class="fa fa-tag"></i> {{ $category->description }}
                                            </div>
                                        @endforeach
                                        <hr>
                                        <!-- //module -->
                                        <!-- accordions -->
                                        <div class="sub-accor">
                                            <h3 class="title-w3-ab text-uppercase mt-5 mb-4 collapse show">
                                                Contenido</h3>
                                            <ul class="accordion css-accordion">
                                                @foreach ($course->modules as $module)
                                                    <li class="accordion-item">
                                                        <input class="accordion-item-input" type="checkbox"
                                                               name="accordion"
                                                               id="item{{ $module->id }}">
                                                        <label for="item{{ $module->id }}" class="accordion-item-hd">
                                                            <div class="row">
                                                                <div
                                                                    class="col-sm-6">{{--<a href="#" aria-current="true">--}}{{ $module->title }}{{--</a>--}}
                                                                    <div class="progress" style="height: 20px;">
                                                                        <div class="progress-bar" role="progressbar"
                                                                             style="width: {{ $module->progress() }}%;"
                                                                             aria-valuenow="25"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"></div>
                                                                    </div>
                                                                </div>
                                                                {{--<div class="col-sm-2">
                                                                    Inicio:<br>
                                                                    23/03/2021
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    Fin:<br>
                                                                    26/03/2021
                                                                </div>--}}
                                                                {{--<div class="col-sm-2">
                                                                    <a href="--}}{{--{{ route('course.lesson') }}--}}{{--"
                                                                       class="btn btn-success t-white">Acceder</a>
                                                                </div>--}}
                                                            </div>
                                                            <span class="accordion-item-hd-cta"><i
                                                                    class="fa fa-chevron-right"></i></span></label>
                                                        <div class="accordion-item-bd accordion-item-bd-2">
                                                            @foreach ($module->lessons as $lesson)
                                                                <div class="form-check">
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <div class="checkbox">
                                                                                <label>
                                                                                    @if ($lesson->userLesson)
                                                                                        <i style="color:green"
                                                                                           class="fa fa-check"></i>
                                                                                    @else
                                                                                        <i class="fa fa-unlock"></i>
                                                                                    @endif
                                                                                        @if (isset(Auth::user()->orders) && Auth::user()->orders->where('course_id', $course->id)->first())
                                                                                                <a href="{{ route('course.lesson', ['course' => $course->slug, 'lesson' => $lesson->slug]) }}">{{ $lesson->title }}</a>
                                                                                        @else
                                                                                                <a href="javascript:void(0)">{{ $lesson->title }}</a>
                                                                                        @endif
                                                                                    {{--<a href="{{ route('course.lesson', ['course' => $course->slug, 'lesson' => $lesson->slug]) }}">{{ $lesson->title }}</a>--}}
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            {{--<h6 class="text-right"><a
                                                                                    href="modulos.html"><i
                                                                                        class=" fa fa-play"></i> 08:30
                                                                                    min</a>
                                                                            </h6>--}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                @if ($loop->last)
                                                                    @if ($eval = $module->evaluation)
                                                                        <hr>
                                                                        <div class="form-check">
                                                                            <div class="row">
                                                                                <div class="col-sm-6">
                                                                                    <div class="checkbox">
                                                                                        <label>
                                                                                            @if ($eval->currentUserEvaluation)
                                                                                                <i style="color:green"
                                                                                                   class="fa fa-check"></i>
                                                                                                <a href="{{ route('course.module.evaluation') }}">Examen</a>
                                                                                            @elseif ($module->progress() < 100)
                                                                                                <i class="fa fa-lock"></i>
                                                                                                <a style="cursor: move;pointer-events: none;"
                                                                                                   href="{{ route('course.module.evaluation') }}">Examen</a>
                                                                                            @else
                                                                                                <i class="fa fa-unlock"></i>
                                                                                                <a href="{{ route('course.module.evaluation') }}">Examen</a>
                                                                                            @endif

                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    {{--<h6 class="text-right">
                                                                                        <a
                                                                                            href="modulos.html"><i
                                                                                                class=" fa fa-play"></i> 08:30
                                                                                            min</a>
                                                                                    </h6>--}}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                @endif
                                                                <hr>
                                                            @endforeach
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                            <!-- //accordions -->
                                        </div>
                                    </div>


                                </div>
                                <!---728x90--->


                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!---728x90--->
@endsection

