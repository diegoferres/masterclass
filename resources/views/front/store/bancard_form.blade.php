﻿@extends('front.layouts.app')
@section('content')
    <!-- inner banner -->

    <!-- //covers -->
    <!---728x90--->
    <section class="wrapper">

        <div class="py-5 text-center  " style="color:#9ac359">
            <h2 class="m-2"><i class="fa fa-credit-card"></i> Información de pago</h2>

        </div>
    </section>
    <section class="mc-ecom-cart mb-5 p-7">
        <div class="col-sm-12 ">
            <center>
                <div class="col-sm-6 ">
                    <div style="height: 300px; width: auto; margin: auto" id="iframe-container"></div>
                </div>
            </center>
        </div>
    </section>
    <!---728x90--->
@endsection
@section('scripts')
    <script src="//m.servedby-buysellads.com/monetization.js" type="text/javascript"></script>
    <script
        src="https://vpos.infonet.com.py:8888/checkout/javascript/dist/bancard-checkout-3.0.0.js"></script>
    <script>
       /* $(document).ready(function () {*/
            window.onload = function () {
                Bancard.Checkout.createForm('iframe-container', '{{ $result->process_id }}');
            };
       /* })*/
    </script>
@endsection
