﻿@extends('front.layouts.app')
@section('content')
    <!-- inner banner -->

    <!-- //covers -->
    <!---728x90--->
    <section class="wrapper">

        <div class="py-1 text-center  " style="color:#9ac359">
            {{--<img class="d-block mx-auto mb-4" width="300" src="/front/assets/images/masquelier_brand.png">
            <hr>--}}
            <h2 class="m-5"><i class="bi-cart"></i> Confirmación de Compra</h2>
            {{--<img class="d-block mx-auto mb-4" width="120" src="{{ asset('front/assets/images/alert_image.png') }}">--}}
            <img class="d-block mx-auto mb-4" width="100" src="{{ asset('front/assets/images/check_image.png') }}">
            <hr>
            {{--<a  href="{{ route('course.show', $course->slug) }}" class="btn btn-success">Volver a intentar</a>--}}

        </div>
    </section>

        <section class="mc-ecom-cart mb-5 p-7">
            <div class="col-sm-12 ">
                <center>
                    <div class="col-sm-12">
                        <div class="shopping-cart m-0">
                        <div class="col-sm-6 text-left">

                            <div class="product">
                                <div class="product-image">
                                </div>
                                <div class="product-details">
                                    <div class="product-title">Fecha/Hora</div>
                                </div>
                                <div class="product-description">{{ $order->created_at }}</div>
                            </div>
                            <div class="product">
                                <div class="product-image">
                                </div>
                                <div class="product-details">
                                    <div class="product-title"># Pedido</div>
                                </div>
                                <div class="product-description">{{ $order->id }}</div>
                            </div>
                            <div class="product">
                                <div class="product-image">
                                </div>
                                <div class="product-details">
                                    <div class="product-title">Monto</div>
                                </div>
                                <div class="product-description">{{ number_format($order->amount, 0, ',', '.') }}</div>
                            </div>
                            <div class="product ">
                                <div class="product-image">
                                </div>
                                <div class="product-details">
                                    <div class="product-title">Estado</div>
                                </div>
                                @if ($order->payment_method_id == 1)
                                    <div class="product-description">{{ json_decode($order->payment)->response_description  }}</div>
                                @else
                                    <div class="product-description">Pendiente de verificación</div>
                                @endif
                            </div>
                            @if ($order->payment_method_id == 1)
                                <div class="product ">
                                    <div class="product-image">
                                    </div>
                                    <div class="product-details">
                                        <div class="product-title">Ticket</div>
                                    </div>
                                    <div class="product-description">{{ json_decode($order->payment)->ticket_number }}</div>
                                </div>
                            @endif
                            <!--fin LISTA-->
                            <div class="form-group  text-right">
                                <a  href="{{ route('course.show', $order->course->slug) }}" class="btn btn-success">Ir al curso</a>
                            </div>
                        </div>
                    </div>
                </center>
            </div>


    </section>
    <!---728x90--->
@endsection
@section('scripts')
    <script src="//m.servedby-buysellads.com/monetization.js" type="text/javascript"></script>
@endsection
