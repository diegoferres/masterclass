﻿@extends('front.layouts.app')
@section('styles')
    @livewireStyles
@endsection
@section('content')
    <!-- inner banner -->

    <!-- //covers -->
    <!---728x90--->
    <section class="wrapper">

        <div class="py-5 text-center  " style="color:#9ac359">
            <img class="d-block mx-auto mb-4" width="300" src="/front/assets/images/masquelier_brand.png">
            <hr>
            <h2 class="m-5"><i class="bi-cart"></i> Confirmación de Compra</h2>
            @if (session()->has('bancard_response'))
                <p class="lead" style="color: orange"><i class="fa fa-warning fa-2x"></i>
                    <br><strong>{{ json_decode(session()->get('bancard_response'))->response_description }}</strong>
                    <br>{{ json_decode(session()->get('bancard_response'))->extended_response_description }}</p>
            @endif
            @if (session()->get('error'))
                <p class="lead" style="color: orange"><i class="fa fa-warning fa-2x"></i>
                    <br><strong>{{ session()->get('error') }}</strong>
                </p>
            @endif
            <hr>
        </div>
    </section>
    @livewire('front.store.checkout', ['course' => $course])
    <!---728x90--->
@endsection
@section('scripts')
    <script src="//m.servedby-buysellads.com/monetization.js" type="text/javascript"></script>
    @livewireScripts
@endsection
