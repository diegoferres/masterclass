﻿@extends('front.layouts.app')
@section('styles')
    @livewireStyles
@endsection
@section('content')
    <!---728x90--->
    <section class="" style="background: #e9eaea">
        <div class="container ">
            <div class="row  ">
                <div class="col-md-12 mt-3 mb-3">
                    <div class="card">
                        @livewire('front.course.module.evaluation', ['eval' => $eval])
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!---728x90--->
@endsection
@section('scripts')
    @livewireScripts
    @stack('livewire_scripts')
@endsection
