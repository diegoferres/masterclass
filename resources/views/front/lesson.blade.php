﻿@extends('front.layouts.app')
@section('styles')
    @livewireStyles
@endsection
@section('content')
    <!---728x90--->
    <section class="" style="background: #e9eaea">

        <div class="container ">
            <div class="col-12">
                <div class="row" style="display: -webkit-inline-flex">
                <div class="col-md-8 mt-3 mb-3">
                    <div class="card">
                        <div class="card-body overflow-hidden" style="max-height: 387px;">

                            @if(explode('/', mime_content_type('storage/'.$lesson->resource_path))[0] == 'image')
                                <center>
                                    <img src="{{asset('storage/'.$lesson->resource_path)}}" class="img-responsive ">
                                </center>
                            @elseif (explode('/', mime_content_type('storage/'.$lesson->resource_path))[0] == 'video')

                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item"
                                            src="{{asset('storage/'.$lesson->resource_path)}}"
                                            allowfullscreen></iframe>
                                </div>
                            @endif
                        </div>
                        <div class="card-footer">
                            @if ($lesson->previous())
                                <a href="{{ route('course.lesson', [$lesson->module->course->slug, $lesson->previous()->slug]) }}"
                                   class="card-link text-success">Anterior</a>
                            @elseif ($lesson->module->previous())
                                <a href="{{ route('course.lesson', [$lesson->module->course->slug, $lesson->module->previous()->lessons()->orderBy('sort','DESC')->first()->slug]) }}"
                                   class="card-link text-success">Anterior</a>
                            @endif

                            @if ($lesson->next())
                                <a href="{{ route('course.lesson', [$lesson->module->course->slug, $lesson->next()->slug]) }}"
                                   class="card-link pull-right text-success">Siguiente</a>
                            @elseif ($lesson->module->next())
                                @if ($lesson->module->next()->lessons->first())
                                    <a href="{{ route('course.lesson', [$lesson->module->course->slug, $lesson->module->next()->lessons->first()->slug]) }}"
                                       class="card-link pull-right text-success">Siguiente</a>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mt-3 mb-3">
                    <div class="card   ">
                        <div class="card-body">
                            <h5 class="card-title">{{ $lesson->module->title }}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">{{ $lesson->title }}</h6>
                            <p class="card-text text-justify">{!! $lesson->description !!}</p>
                            <hr>
                            <p>Adjuntos:</p>
                            <p>
                                @foreach ($lesson->files as $file)
                                    <a target="_blank" href="{{ asset('storage/'.$file->filepath) }}"
                                       class="card-link text-success"><i class="fa fa-file"></i>
                                        {{ $file->filename }}</a>
                                    <br>
                                @endforeach

                                {{--<a href="#" class="card-link text-success  "><i class="fa fa-file-pdf-o"></i> Ejercicios</a>--}}
                            </p>
                            <div class="card-footer text-center">
                                @if ($lesson->userLesson)
                                    <a class="btn btn-default"><i class="fa fa-check"></i> Finalizado
                                    </a>
                                @else
                                    <a href="{{ route('course.lesson.finish', ['lesson' => $lesson->id]) }}"
                                       class="btn btn-success"><i class="fa fa-check"></i> Marcar como terminado
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-12  mt-3 mb-3 ">
                    <div class="card">
                        <div class="card-body">
                            <section id="tabs">
                                <div class="container  ">
                                    <div class="row">
                                        <div class="col-sm-12 ">
                                            <nav>
                                                <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                                    <a class="nav-item nav-link active  " id="nav-contenido-tab"
                                                       data-toggle="tab"
                                                       href="#nav-contenido" role="tab" aria-controls="nav-home"
                                                       aria-selected="true">Contenido</a>
                                                    {{--<a class="nav-item nav-link  " id="nav-descripcion-tab"
                                                       data-toggle="tab"
                                                       href="#nav-descripcion" role="tab" aria-controls="nav-profile"
                                                       aria-selected="false">Descripción</a>--}}
                                                    <a class="nav-item nav-link   " id="nav-comentarios-tab"
                                                       data-toggle="tab"
                                                       href="#nav-comentarios" role="tab" aria-controls="nav-contact"
                                                       aria-selected="false">Comentarios</a>
                                                </div>
                                            </nav>
                                            <div class="tab-content " id="nav-tabContent">
                                                <div class="tab-pane fade show active" id="nav-contenido"
                                                     role="tabpanel"
                                                     aria-labelledby="nav-contenido-tab">

                                                    <div class="mc-module-main">
                                                        <div class="module-sec">
                                                            <div class="wrapper">
                                                                <!-- module -->
                                                                <div class="d-grid module-cont">
                                                                    <div class="about-right-module">

                                                                        <!-- //module -->
                                                                        <!-- accordions -->
                                                                        <div class="sub-accor">
                                                                            <h3 class="title-w3-ab text-uppercase mt-5 mb-4">
                                                                                Contenido</h3>
                                                                            <ul class="accordion css-accordion">
                                                                                @foreach ($lesson->module->course->modules as $module)
                                                                                    <li class="accordion-item">
                                                                                        <input
                                                                                            @if ($module->id == $lesson->module_id)
                                                                                            checked
                                                                                            @endif
                                                                                            class="accordion-item-input"
                                                                                            type="checkbox"
                                                                                            name="accordion"
                                                                                            id="item{{ $module->id }}">
                                                                                        <label
                                                                                            for="item{{ $module->id }}"
                                                                                            class="accordion-item-hd">
                                                                                            <div class="row">
                                                                                                <div
                                                                                                    class="col-sm-6">{{--<a href="#" aria-current="true">--}}{{ $module->title }}{{--</a>--}}
                                                                                                    <div
                                                                                                        class="progress"
                                                                                                        style="height: 20px;">
                                                                                                        <div
                                                                                                            class="progress-bar"
                                                                                                            role="progressbar"
                                                                                                            style="width: {{ $module->progress() }}%;"
                                                                                                            aria-valuenow="0"
                                                                                                            aria-valuemin="0"
                                                                                                            aria-valuemax="100"></div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <span
                                                                                                class="accordion-item-hd-cta"><i
                                                                                                    class="fa fa-chevron-right"></i></span></label>
                                                                                        <div
                                                                                            class="accordion-item-bd accordion-item-bd-2">
                                                                                            @foreach ($module->lessons as $less)
                                                                                                {{--@dd($module->lessons)--}}
                                                                                                <div class="form-check">
                                                                                                    <div class="row">
                                                                                                        <div
                                                                                                            class="col-sm-6">
                                                                                                            <div
                                                                                                                class="checkbox">
                                                                                                                <label>
                                                                                                                    @if ($less->userLesson)
                                                                                                                        <i style="color:green"
                                                                                                                           class="fa fa-check"></i>
                                                                                                                    @else
                                                                                                                        <i class="fa fa-unlock"></i>
                                                                                                                    @endif
                                                                                                                    <a href="{{ route('course.lesson', ['course' => $less->slug, 'lesson' => $less->slug]) }}">{{ $less->title }}</a>
                                                                                                                </label>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div
                                                                                                            class="col-sm-6">
                                                                                                            {{--<h6 class="text-right">
                                                                                                                <a
                                                                                                                    href="modulos.html"><i
                                                                                                                        class=" fa fa-play"></i>
                                                                                                                    08:30
                                                                                                                    min</a>
                                                                                                            </h6>--}}
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                @if ($loop->last)
                                                                                                    @if ($eval = $module->evaluation)
                                                                                                        <hr>
                                                                                                        <div
                                                                                                            class="form-check">
                                                                                                            <div
                                                                                                                class="row">
                                                                                                                <div
                                                                                                                    class="col-sm-6">
                                                                                                                    <div
                                                                                                                        class="checkbox">
                                                                                                                        <label>
                                                                                                                            @if ($eval->currentUserEvaluation)
                                                                                                                                <i style="color:green"
                                                                                                                                   class="fa fa-check"></i>
                                                                                                                                <a href="{{ route('course.module.evaluation') }}">Examen</a>
                                                                                                                            @elseif ($module->progress() < 100)
                                                                                                                                <i class="fa fa-lock"></i>
                                                                                                                                <a style="cursor: move;pointer-events: none;" href="{{ route('course.module.evaluation') }}">Examen</a>
                                                                                                                            @else
                                                                                                                                <i class="fa fa-unlock"></i>
                                                                                                                                <a href="{{ route('course.module.evaluation') }}">Examen</a>
                                                                                                                            @endif
                                                                                                                        </label>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div
                                                                                                                    class="col-sm-6">
                                                                                                                    {{--<h6 class="text-right">
                                                                                                                        <a
                                                                                                                            href="modulos.html"><i
                                                                                                                                class=" fa fa-play"></i> 08:30
                                                                                                                            min</a>
                                                                                                                    </h6>--}}
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    @endif
                                                                                                @endif
                                                                                                <hr>
                                                                                            @endforeach
                                                                                        </div>
                                                                                    </li>
                                                                                @endforeach
                                                                            </ul>
                                                                            <!-- //accordions -->
                                                                        </div>
                                                                    </div>


                                                                </div>
                                                                <!---728x90--->


                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <hr>
                                                    <br>
                                                </div>
                                                <div class="tab-pane fade" id="nav-descripcion" role="tabpanel"
                                                     aria-labelledby="nav-descripcion-tab">


                                                    <div class="mc-module-main">
                                                        <div class="module-sec">
                                                            <div class="wrapper">
                                                                <!-- module -->
                                                                <div class="d-grid module-cont">
                                                                    <div class="about-right-module">


                                                                        <!-- //module -->
                                                                        <!-- accordions -->
                                                                        <div class="sub-accor">
                                                                            <h3 class="title-w3-ab text-uppercase mt-5 mb-4">
                                                                                Lección
                                                                                1.</h3>
                                                                            <p>detalles del curso en cuestion</p>
                                                                            <!-- //accordions -->
                                                                        </div>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <hr>
                                                                        <br>
                                                                    </div>


                                                                </div>
                                                                <!---728x90--->


                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="tab-pane fade" id="nav-comentarios" role="tabpanel"
                                                     aria-labelledby="nav-comentarios-tab">
                                                    <div class="card">

                                                        {{-- Nivel 1 --}}
                                                        {{--<div
                                                            class="[ panel panel-default ] panel-google-plus m-2 comments-respuesta   mt-0 mr-0 mb-0 ">
                                                            <div
                                                                class="panel-heading ml-5 mt-0 mr-0 mb-0 pl-2   rounded   "
                                                                style="background: #EEEEEE"> --}}{{--  comments-doc--}}{{--
                                                                <img class=" rounded-circle pull-left"
                                                                     style="position:absolute; left: -2px; top: 0px "
                                                                     src="https://lh3.googleusercontent.com/-CxXg7_7ylq4/AAAAAAAAAAI/AAAAAAAAAQ8/LhCIKQC5Aq4/s46-c-k-no/photo.jpg"
                                                                     alt="Mouse0270"/>
                                                                <div class="panel-body  ">
                                                                    <div class="text-justify p-2 small ">
                                                                        Texto texto texto texto texto texto texto texto
                                                                        texto texto texto texto texto texto texto texto
                                                                        texto texto
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel-body ml-4 pl-4">
                                                                <span class="p-1 ">Diego Armando Maradona  </span> |
                                                                <span class="text-gray" style="color: #999">comentado el 27, marzo 2021</span>
                                                            </div>
                                                        </div>--}}


                                                        @livewire('front.lessons.comments', ['lesson' => $lesson])
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>

    </section>
    <!---728x90--->
@endsection
@section('scripts')
    @livewireScripts
    <script>
        $(function () {
            $('.panel-google-plus > .panel-footer > .input-placeholder, .panel-google-plus > .panel-google-plus-comment > .panel-google-plus-textarea > button[type="reset"]').on('click', function (event) {
                var $panel = $(this).closest('.panel-google-plus');
                $comment = $panel.find('.panel-google-plus-comment');

                $comment.find('.btn:first-child').addClass('disabled');
                $comment.find('textarea').val('');

                $panel.toggleClass('panel-google-plus-show-comment');

                if ($panel.hasClass('panel-google-plus-show-comment')) {
                    $comment.find('textarea').focus();
                }
            });
            $('.panel-google-plus-comment > .panel-google-plus-textarea > textarea').on('keyup', function (event) {
                var $comment = $(this).closest('.panel-google-plus-comment');

                $comment.find('button[type="submit"]').addClass('disabled');
                if ($(this).val().length >= 1) {
                    $comment.find('button[type="submit"]').removeClass('disabled');
                }
            });
        });
    </script>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
            crossorigin="anonymous"></script>
@endsection
