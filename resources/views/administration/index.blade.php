@extends('administration.layout.app')
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                Inicio
            </h3>
        </div>
        <div class="row grid-margin">
            <div class="col-12">
                <div class="card card-statistics">
                    <div class="card-body">
                        <div class="d-flex flex-column flex-md-row align-items-center justify-content-between">
                            <div class="statistics-item">
                                <p>
                                    <i class="icon-sm fa fa-user mr-2"></i>
                                    Alumnos
                                </p>
                                <h2>{{ $students->count() }}</h2>
                                {{--<label class="badge badge-outline-success badge-pill">2.7% increase</label>--}}
                            </div>
                            <div class="statistics-item">
                                <p>
                                    <i class="icon-sm fas fa-video mr-2"></i>
                                    Cursos
                                </p>
                                <h2>{{ $courses->count() }}</h2>
                                {{--<label class="badge badge-outline-danger badge-pill">30% decrease</label>--}}
                            </div>
                            <div class="statistics-item">
                                <p>
                                    <i class="icon-sm fas fa-chart-line mr-2"></i>
                                    Total compras
                                </p>
                                <h2>Gs. {{ number_format($payments->sum('amount'), 0, ',','.') }}</h2>
                                {{--<label class="badge badge-outline-success badge-pill">12% increase</label>--}}
                            </div>
                            {{--<div class="statistics-item">
                                <p>
                                    <i class="icon-sm fas fa-check-circle mr-2"></i>
                                    Update
                                </p>
                                <h2>7500</h2>
                                <label class="badge badge-outline-success badge-pill">57% increase</label>
                            </div>
                            <div class="statistics-item">
                                <p>
                                    <i class="icon-sm fas fa-chart-line mr-2"></i>
                                    Sales
                                </p>
                                <h2>9000</h2>
                                <label class="badge badge-outline-success badge-pill">10% increase</label>
                            </div>
                            <div class="statistics-item">
                                <p>
                                    <i class="icon-sm fas fa-circle-notch mr-2"></i>
                                    Pending
                                </p>
                                <h2>7500</h2>
                                <label class="badge badge-outline-danger badge-pill">16% decrease</label>
                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row grid-margin">
            <div class="col-4">
                <div class="card">
                    <div class="card-body d-flex flex-column">
                        <h4 class="card-title">
                            <i class="fas fa-chart-pie"></i>
                            Alumnos por curso
                        </h4>
                        <div class="flex-grow-1 d-flex flex-column justify-content-between"><div class="chartjs-size-monitor" style="position: absolute; inset: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                            <canvas id="sales-status-chart" class="mt-3 chartjs-render-monitor" width="425" height="211" style="display: block; height: 169px; width: 340px;"></canvas>
                            <div class="pt-4">
                                <div id="sales-status-chart-legend" class="sales-status-chart-legend"><ul class="legend2"><li><span class="legend-label" style="background-color:#392c70"></span>Active users<label class="badge badge-light badge-pill legend-percentage ml-auto">75%</label></li><li><span class="legend-label" style="background-color:#04b76b"></span>Subscribers<label class="badge badge-light badge-pill legend-percentage ml-auto">25%</label></li><li><span class="legend-label" style="background-color:#ff5e6d"></span>New visitors<label class="badge badge-light badge-pill legend-percentage ml-auto">15%</label></li><li><span class="legend-label" style="background-color:#eeeeee"></span>Others<label class="badge badge-light badge-pill legend-percentage ml-auto">10%</label></li></ul></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-8">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">
                            <i class="fas fa-user-alt"></i>
                            Ultimos inscriptos
                        </h4>
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                @foreach ($students->sortByDesc('created_at') as $student)
                                    <tr>
                                        <td class="py-1">
                                            <img src="https://ui-avatars.com/api/?name={{ $student->name }}" alt="profile" class="img-sm rounded-circle">
                                        </td>
                                        <td class="font-weight-bold">
                                            {{ $student->name }}
                                        </td>
                                        <td>
                                            @foreach ($student->orders as $order)
                                                <label class="badge badge-light badge-pill">{{ $order->course->title }}{{ $order->course->title }}</label>
                                                @if (!$loop->last)
                                                    <br>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            {{ $student->created_at }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<!-- Custom js for this page-->
<script src="/administration/js/dashboard.js"></script>
<script>
    $(document).ready(function () {
        if ($("#sales-status-chart").length) {
            var pieChartCanvas = $("#sales-status-chart").get(0).getContext("2d");
            var pieChart = new Chart(pieChartCanvas, {
                type: 'pie',
                data: {
                    datasets: [{
                        data: [
                            @isset($courses)
                            @foreach ($courses as $course)
                                {{ $course->orders->where('paid', true)->count() }},
                            @endforeach
                            @endisset
                            /*75,
                            25,
                            15,
                            10*/
                        ],
                        @foreach ($courses as $course)
                            @php $arrayColor[] = (str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT)) . (str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT)) . (str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT)) @endphp
                        @endforeach
                            backgroundColor: [
                                @isset($arrayColor)
                                    @foreach ($arrayColor as $color)
                                            '#{{ $color }}',
                                    @endforeach
                                @endisset
                            ],
                            borderColor: [
                                @isset($arrayColor)
                                @foreach ($arrayColor as $color)
                                    '#{{ $color }}',
                                @endforeach
                                @endisset
                                /*'#04b76b',
                                '#ff5e6d',
                                '#eeeeee'*/
                            ],

                    }],

                    // These labels appear in the legend and in the tooltips when hovering different arcs
                    labels: [
                        @foreach ($courses as $course)
                            '{{ $course->title }}',
                        @endforeach

                        /*'Subscribers',
                        'New visitors',
                        'Others'*/
                    ]
                },
                options: {
                    responsive: true,
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    },
                    legend: {
                        display: true
                    },
                    legendCallback: function(chart) {
                        var text = [];
                        text.push('<ul class="legend'+ chart.id +'">');
                        for (var i = 0; i < chart.data.datasets[0].data.length; i++) {
                            text.push('<li><span class="legend-label" style="background-color:' + chart.data.datasets[0].backgroundColor[i] + '"></span>');
                            if (chart.data.labels[i]) {
                                text.push(chart.data.labels[i]);
                            }
                            text.push('<label class="badge badge-light badge-pill legend-percentage ml-auto">'+ chart.data.datasets[0].data[i] + '<!--%--></label>');
                            text.push('</li>');
                        }
                        text.push('</ul>');
                        return text.join("");
                    }
                }
            });
            document.getElementById('sales-status-chart-legend').innerHTML = pieChart.generateLegend();
        }
    })

</script>
<!-- End custom js for this page-->
@endsection

