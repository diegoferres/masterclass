<!DOCTYPE html>
<html lang="en">

<head>
    @include('administration.partials.head')
</head>
<body @if (\Request::route()->getName() == 'admin.courses.edit')
      class="sidebar-icon-only"
@endif>
<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    @include('administration.partials.nav')
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        @include('administration.partials.sidebar')
        <div class="main-panel">
            @yield('content')
            <!-- content-wrapper ends -->
            <!-- partial:partials/_footer.html -->
            <footer class="footer">
                <div class="d-sm-flex justify-content-center justify-content-sm-between">
                    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2018. All rights reserved.</span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i
                            class="far fa-heart text-danger"></i></span>
                </div>
            </footer>
            <!-- partial -->
        </div>
        <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
@include('administration.partials.scripts')
</body>


</html>
