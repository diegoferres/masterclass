@extends('administration.layout.app')
@section('styles')

    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                Usuarios
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Admin</a></li>
                    <li class="breadcrumb-item" aria-current="page">Usuarios</li>
                    <li class="breadcrumb-item active" aria-current="page">Crear</li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        @isset ($user)
                            <h4 class="card-title">Editar curso</h4>
                        @else
                            <h4 class="card-title">Crear curso</h4>
                        @endisset
                        @isset ($user)
                            {!! Form::model($user, ['route' => ['admin.users.update', $user->id], 'method' => 'PATCH','files' => true]) !!}
                        @else
                            {!! Form::open(['route' => 'admin.users.store', 'method' => 'post' ,'files' => true]) !!}
                        @endisset

                        <div class="form-group row">
                            <div class="col-sm-5">
                                <label>Nombre y apellido:</label>
                                {!! Form::text('name', old('name'), ['class' => 'form-control form-control-sm'.($errors->has('name') ? ' is-invalid': '')]) !!}
                                @error('name')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="col-sm-3">
                                <label>Correo:</label>
                                @if ($user)
                                    {!! Form::text('email', old('email'), ['class' => 'form-control form-control-sm', 'readonly']) !!}
                                @else
                                    {!! Form::text('email', old('email'), ['class' => 'form-control form-control-sm']) !!}
                                @endif
                            </div>
                            <div class="col-sm-2">
                                <label>Roles:</label>
                                {!! Form::select('roles[]', $roles , isset($user) ? $user->roles->pluck('id') : old('roles'), ['class' => 'js-example-basic-multiple form-control'.($errors->has('roles') ? ' is-invalid': ''), 'multiple', 'style' => 'width:100%']) !!}
                                @error('roles')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label>Contraseña:</label>
                                {!! Form::password('password', ['class' => 'form-control'.($errors->has('password') ? ' is-invalid': '')]) !!}
                                @error('password')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="col-sm-3">
                                <label>Confirmar contraseña:</label>
                                {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                                @error('price')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary mr-2">Guardar</button>
                        <a href="{{ route('admin.categories.index') }}" class="btn btn-light">Cancelar</a>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="\administration\js\select2.js"></script>
    <script src="https://cdn.tiny.cloud/1/le0xnl72caevg2xl8a4noobxfd3cuv10352omp6pa0ieawfi/tinymce/5/tinymce.min.js"
            referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            menubar: false,
            height: 350,
            selector: 'textarea',
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount'
            ],
            toolbar: 'undo redo | bold italic underline strikethrough ' +
                '| fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | ' +
                'outdent indent |  numlist bullist | forecolor backcolor removeformat | emoticons | fullscreen  preview',
        });
    </script>
    <script type="text/javascript">
        // Upload Images
        $('.products_uploader').change(function () {
            var imageContainer = $('#preview_images');
            imageContainer.empty();
            console.log('Upload Images');
            var quantity = this.files.length;
            console.log('Quantity: ' + quantity);
            var anyWindow = window.URL || window.webkitURL;
            for (var i = 0; i < quantity; i++) {
                console.log('Create a new image for index ' + i);
                var objectUrl = anyWindow.createObjectURL(this.files[i]);
                imageContainer.append('<div class="col-md-2 product_image" id="image_' + i + '" data-id="' + i + '">' +
                    '<img class="image_preview" style="width: 150px; height: 150px" src="' + objectUrl + '" />' +
                    '</div>');
                window.URL.revokeObjectURL(this.files[i]);
            }
        });
        // Select main image
        $(document).on('click', 'img.image_preview', function (e) {
            $('#preview_images .product_image img').removeClass('selected');
            var data_id = $(this).parent().data('id');
            console.log('New main image: ' + data_id);
            $(this).addClass('selected');
            $('#main_image').val(data_id);
        });
    </script>
@endsection

