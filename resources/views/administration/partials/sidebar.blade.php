<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            <div class="nav-link">
                <div class="profile-image">
                    <img src="/front/assets/images/logo_white.png" alt="image"/>
                </div>
                <div class="profile-name">
                    <p class="name">
                        {{ Auth::user()->name }}
                    </p>
                    <p class="designation">
                        {{ Auth::user()->email }}
                    </p>
                </div>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.dashboard') }}">
                <i class="fa fa-tablet menu-icon"></i>
                <span class="menu-title">Tablero</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.orders.index') }}">
                <i class="fa fa-money-check menu-icon"></i>
                <span class="menu-title">Órdenes</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.categories.index') }}">
                <i class="fa fa-list menu-icon"></i>
                <span class="menu-title">Categorías</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.courses.index') }}">
                <i class="fa fa-file-pdf menu-icon"></i>
                <span class="menu-title">Cursos</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.students.index') }}">
                <i class="fa fa-graduation-cap menu-icon"></i>
                <span class="menu-title">Alumnos</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.comments.index') }}">
                <i class="fa fa-comment menu-icon"></i>
                <span class="menu-title">Comentarios</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.users.index') }}">
                <i class="fa fa-users menu-icon"></i>
                <span class="menu-title">Usuarios</span>
            </a>
        </li>
        {{--<li class="nav-item">
            <a class="nav-link" href="{{ route('admin.news.index') }}">
                <i class="fa fa-video menu-icon"></i>
                <span class="menu-title">Cursos</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.news.index') }}">
                <i class="fa fa-user-circle menu-icon"></i>
                <span class="menu-title">Clientes</span>
            </a>
        </li>

        <li class="nav-item {{ \Illuminate\Support\Facades\Request::route()->getName() == 'admin.a' ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('admin.news.index') }}">
                <i class="fa fa-check menu-icon"></i>
                <span class="menu-title">Orders</span>
            </a>
        </li>--}}
    </ul>
</nav>
<!-- partial -->

















