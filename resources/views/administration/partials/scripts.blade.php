
<!-- plugins:js -->
<script src="/administration/vendors/js/vendor.bundle.base.js"></script>
<script src="/administration/vendors/js/vendor.bundle.addons.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="/administration/js/off-canvas.js"></script>
<script src="/administration/js/hoverable-collapse.js"></script>
<script src="/administration/js/misc.js"></script>
<script src="/administration/js/settings.js"></script>
<script src="/administration/js/todolist.js"></script>

<script src="/administration/js/toastDemo.js"></script>
<script>

@if (session('notification'))

@php($not = json_decode(session('notification')))
{{--@dd($not->type)--}}

    @switch($not->type)
        @case('success')
            showSuccessToast('{{$not->message}}')
        @break

        @case('error')
            showDangerToast('{{$not->message}}')
        @break
    @endswitch
@endif
</script>

<!-- endinject -->
@yield('scripts')
@stack('pushed_scripts')
