@extends('administration.layout.app')
@section('styles')

    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    @livewireStyles
    <style>
        input[type=file] {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            width: 150px;
        }
    </style>
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                Cursos
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Admin</a></li>
                    <li class="breadcrumb-item" aria-current="page">Cursos</li>
                    <li class="breadcrumb-item active" aria-current="page">Editar</li>
                </ol>
            </nav>
        </div>
        @livewire('edit-course', ['course' => $course, 'categories' => $categories])
    </div>


@endsection
@section('scripts')
    <script src="\administration\js\select2.js"></script>
    <script src="https://cdn.tiny.cloud/1/le0xnl72caevg2xl8a4noobxfd3cuv10352omp6pa0ieawfi/tinymce/5/tinymce.min.js"
            referrerpolicy="origin"></script>
    <script>
        $(document).ready(function () {


            window.addEventListener('editLesson',function(e){
                tinymce.get('lesson_description').setContent(e.detail);

            })
        })

    </script>
    <script type="text/javascript">
        // Upload Images
        $('.products_uploader').change(function () {
            var imageContainer = $('#preview_images');
            imageContainer.empty();
            console.log('Upload Images');
            var quantity = this.files.length;
            console.log('Quantity: ' + quantity);
            var anyWindow = window.URL || window.webkitURL;
            for (var i = 0; i < quantity; i++) {
                console.log('Create a new image for index ' + i);
                var objectUrl = anyWindow.createObjectURL(this.files[i]);
                imageContainer.append('<div class="col-md-2 product_image" id="image_' + i + '" data-id="' + i + '">' +
                    '<img class="image_preview" style="width: 150px; height: 150px" src="' + objectUrl + '" />' +
                    '</div>');
                window.URL.revokeObjectURL(this.files[i]);
            }
        });
        // Select main image
        $(document).on('click', 'img.image_preview', function (e) {
            $('#preview_images .product_image img').removeClass('selected');
            var data_id = $(this).parent().data('id');
            console.log('New main image: ' + data_id);
            $(this).addClass('selected');
            $('#main_image').val(data_id);
        });
    </script>
    @livewireScripts
    @stack('livewire_scripts')
    <script>
        window.addEventListener('alert',function(e){
            alert()
            //Notifications from Livewire
            switch (e.detail['type']) {
                case 'success':
                    showSuccessToast(e.detail.message)

                    break;

                case 'error':
                    showDangerToast(e.detail.message)
                    break;

                case 'info':
                    showInfoToast(e.detail.message)
                    break;

                case 'warning':
                    showWarningToast(e.detail.message)
                    break;
            }
            //END Notifications from Livewire
        })

    </script>
    <script src="/administration/js/livewire-sortable.js"></script>
    {{--<script src="/administration/js/dragula.js"></script>--}}
@endsection

