@extends('administration.layout.app')
@section('styles')
    @livewireStyles
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                Alumnos
            </h3>
        </div>
        <div class="row">
            <div class="col-md-8 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">
                            <i class="fas fa-newspaper"></i>
                            Alumnos
                            <a href="{{ route('admin.courses.create') }}" class="fa-pull-right btn btn-info btn-xs"><i
                                    class="fas fa-plus"></i></a>
                        </h4>
                        @livewire('admin.students.index')
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('scripts')
    @livewireScripts
@endsection
