@extends('administration.layout.app')
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                Categorias
            </h3>
        </div>
        <div class="row">
            <div class="col-md-8 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">
                            <i class="fas fa-newspaper"></i>
                            Categorias
                            <a href="{{ route('admin.categories.create') }}" class="fa-pull-right btn btn-info btn-xs"><i class="fas fa-plus"></i></a>
                        </h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Categoria</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($categories as $category)
                                    <tr>
                                        <td class="font-weight-bold">
                                            {{ $category->id }}
                                        </td>
                                        <td class="text-muted">
                                            {{ $category->description }}
                                        </td>
                                        <td>
                                            {!! Form::open(['route' => ['admin.categories.destroy', $category->id], 'method' => 'DELETE']) !!}
                                                <button type="submit" class="btn btn-outline-danger btn-xs"><i class="fas fa-trash"></i></button>
                                            {!! Form::close() !!}
                                            <a href="{{ route('admin.categories.edit', $category->id) }}" class="btn btn-outline-info btn-xs"><i class="fas fa-pencil-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </div>
@endsection

