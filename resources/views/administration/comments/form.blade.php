@extends('administration.layout.app')
@section('styles')

    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                Cursos
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Admin</a></li>
                    <li class="breadcrumb-item" aria-current="page">Cursos</li>
                    <li class="breadcrumb-item active" aria-current="page">Crear</li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        @isset ($course)
                            <h4 class="card-title">Editar curso</h4>
                        @else
                            <h4 class="card-title">Crear curso</h4>
                        @endisset
                        <p class="card-description">
                            Carga la información del curso
                        </p>
                        @isset ($course)
                            {!! Form::model($course, ['route' => ['admin.courses.update', $course->id], 'method' => 'PATCH','files' => true]) !!}
                        @else
                            {!! Form::open(['route' => 'admin.courses.store', 'method' => 'post' ,'files' => true]) !!}
                        @endisset

                        <div class="form-group row">
                            <div class="col-sm-5">
                                <label>Título (requerido *):</label>
                                {!! Form::text('title', old('title'), ['class' => 'form-control form-control-sm'.($errors->has('title') ? ' is-invalid': '')]) !!}
                                @error('title')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="col-sm-3">
                                <label>Categorías (requerido *):</label>
                                {!! Form::select('categories[]', $categories , isset($course) ? $course->categories->pluck('id') : old('categories'), ['class' => 'js-example-basic-multiple form-control'.($errors->has('categories') ? ' is-invalid': ''), 'multiple', 'style' => 'width:100%']) !!}
                                @error('categories')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="col-sm-2">
                                <label>Fecha/Hora Inicio (opcional):</label>
                                {{ Form::input('dateTime-local', 'datetime_start', isset($course) ? strftime('%Y-%m-%dT%H:%M', strtotime($course->datetime_start)) : old('datetime_start'), ['class' => 'form-control form-control-sm'.($errors->has('datetime_start') ? ' is-invalid': '')]) }}
                                @error('datetime_start')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="col-sm-2">
                                <label>Fecha/Hora Fin (opcional):</label>
                                {{ Form::input('dateTime-local', 'datetime_end', isset($course) ? strftime('%Y-%m-%dT%H:%M', strtotime($course->datetime_end)) : old('datetime_end'), ['class' => 'form-control form-control-sm'.($errors->has('datetime_end') ? ' is-invalid': '')]) }}
                                @error('datetime_end')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label>Precio (requerido *):</label>
                                {!! Form::number('price', null, ['class' => 'form-control form-control-sm'.($errors->has('price') ? ' is-invalid': '')]) !!}
                                @error('price')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="col-sm-5">
                                <label>Imagen:</label>
                                <div class="col-sm-12">
                                    <input type="file" name="intro_resource" class="form-control form-control-sm @error('intro_resource') is-invalid @enderror"
                                           accept="image/*,video/*">
                                    @error('intro_resource')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="exampleTextarea1">Descripción (requerido *)</label>
                                    {!! Form::textarea('description', old('description') ?? isset($course) ? $course->description : '', ['rows' => '3','class' => 'form-control'.($errors->has('description') ? ' is-invalid': ''), 'id' => 'tinyEditor']) !!}
                                    @error('description')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                    {{--<textarea name="content_text" id="summernote"></textarea>--}}
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary mr-2">Guardar</button>
                        <a href="{{ route('admin.categories.index') }}" class="btn btn-light">Cancelar</a>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="\administration\js\select2.js"></script>
    <script src="https://cdn.tiny.cloud/1/le0xnl72caevg2xl8a4noobxfd3cuv10352omp6pa0ieawfi/tinymce/5/tinymce.min.js"
            referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            menubar: false,
            height: 350,
            selector: 'textarea',
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount'
            ],
            toolbar: 'undo redo | bold italic underline strikethrough ' +
                '| fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | ' +
                'outdent indent |  numlist bullist | forecolor backcolor removeformat | emoticons | fullscreen  preview',
        });
    </script>
    <script type="text/javascript">
        // Upload Images
        $('.products_uploader').change(function () {
            var imageContainer = $('#preview_images');
            imageContainer.empty();
            console.log('Upload Images');
            var quantity = this.files.length;
            console.log('Quantity: ' + quantity);
            var anyWindow = window.URL || window.webkitURL;
            for (var i = 0; i < quantity; i++) {
                console.log('Create a new image for index ' + i);
                var objectUrl = anyWindow.createObjectURL(this.files[i]);
                imageContainer.append('<div class="col-md-2 product_image" id="image_' + i + '" data-id="' + i + '">' +
                    '<img class="image_preview" style="width: 150px; height: 150px" src="' + objectUrl + '" />' +
                    '</div>');
                window.URL.revokeObjectURL(this.files[i]);
            }
        });
        // Select main image
        $(document).on('click', 'img.image_preview', function (e) {
            $('#preview_images .product_image img').removeClass('selected');
            var data_id = $(this).parent().data('id');
            console.log('New main image: ' + data_id);
            $(this).addClass('selected');
            $('#main_image').val(data_id);
        });
    </script>
@endsection

