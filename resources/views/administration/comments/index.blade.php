@extends('administration.layout.app')
@section('styles')
    <style>
        .upperleft {
            display: block;
            text-align: left;
            vertical-align: top;
        }
        .middlecenter {
            display: block;
            text-align: center;
            vertical-align: middle;
        }
        .lowercenter {
            display: block;
            text-align: center;
            vertical-align: bottom;
        }
    </style>
    @livewireStyles
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                Comentarios
            </h3>
        </div>
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">
                            <i class="fas fa-newspaper"></i>
                            Comentarios
                            <a href="{{ route('admin.courses.create') }}" class="fa-pull-right btn btn-info btn-xs"><i
                                    class="fas fa-plus"></i></a>
                        </h4>
                        @livewire('admin.comments.index')
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('scripts')
    @livewireScripts
@endsection
