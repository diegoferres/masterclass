@extends('administration.layout.app')
@section('styles')
    @livewireStyles()
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                Ordenes
            </h3>
        </div>
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    @livewire('admin.orders.index')
                </div>
            </div>
        </div>
@endsection
@section('scripts')
    @livewireScripts
@endsection

