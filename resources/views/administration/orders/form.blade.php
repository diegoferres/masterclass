@extends('administration.layout.app')
@section('styles')

    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                Categorias
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Admin</a></li>
                    <li class="breadcrumb-item" aria-current="page">Categorias</li>
                    <li class="breadcrumb-item active" aria-current="page">Crear</li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        @isset ($category)
                            <h4 class="card-title">Editar noticia noticia</h4>
                        @else
                            <h4 class="card-title">Crear noticia</h4>
                        @endisset
                        <p class="card-description">
                            Carga la información de las últimas noticias
                        </p>
                        @isset ($category)
                            {!! Form::model($category, ['route' => ['admin.categories.update', $category->id], 'method' => 'PATCH','files' => true]) !!}
                        @else
                            {!! Form::open(['route' => 'admin.categories.store', 'method' => 'post' ,'files' => true]) !!}
                        @endisset

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label>Description:</label>
                                {!! Form::text('description', old('description'), ['class' => 'form-control', 'required']) !!}
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary mr-2">Guardar</button>
                        <a href="{{ route('admin.categories.index') }}" class="btn btn-light">Cancelar</a>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

