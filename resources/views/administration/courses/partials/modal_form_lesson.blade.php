<div wire:ignore.self class="modal fade" id="exampleModal-4" tabindex="-1" role="dialog"
     aria-labelledby="ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalLabel">@if ($lessonSelected) {{ $lessonSelected->title }} @else Nueva lección @endif</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if ($lessonSelected)
                    {!! Form::open(['route' => ['admin.lessons.update', $lessonSelected->id], 'method' => 'patch', 'files' => true]) !!}
                @else
                    {!! Form::open(['route' => 'admin.lessons.store', 'method' => 'post', 'files' => true]) !!}
                @endif
                <input type="hidden" name="course_id" value="{{ $course->id }}" >
                <input type="hidden" name="module_id" value="{{ !$module ?: $module->id }}">
                <div class="form-group row">
                    <div class="col-sm-12">
                        <label>Título (requerido *):</label>
                        {!! Form::text('title', old('title'), ['wire:model' => 'lesson_title','class' => 'form-control form-control-sm'.($errors->has('title') ? ' is-invalid': '')]) !!}
                        @error('title')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-5">
                        <label>Vídeo/Imagen de la lección (requerido *):</label>
                        {!! Form::file('lesson_resource', ['wire:model' => 'lesson_resource','class' => 'form-control form-control-sm'.($errors->has('lesson_resources') ? ' is-invalid': '')]) !!}
                        @error('lesson_resources')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="col-sm-7">
                        Vista previa:
                        <br>
                        @if ($lesson_resource)
                            @if (is_string($lesson_resource))
                                <a target="_blank" href="{{ asset('storage/'.$lesson_resource) }}"
                                   class="card-link text-success"><i class="fas fa-file"></i>
                                    {{ 'Recurso principal' }}</a>
                            @else

                                <a target="_blank" href="{{ $lesson_resource->temporaryUrl() }}"
                                   class="card-link text-success"><i class="fas fa-file"></i>
                                    {{ $lesson_resource->getClientOriginalName() }}</a>
                            @endif

                        @endif
                    </div>
                </div>
                <div class="form-group row">

                    <div class="col-sm-5">
                        {!! Form::file('resources[]', ['wire:model' => 'resources','multiple','class' => 'form-control form-control-sm'.($errors->has('resources') ? ' is-invalid': '')]) !!}
                        @error('resources')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="col-sm-7">
                        Recursos:
                        <br>
                        @if ($resources)
                            <table class="table">
                                @foreach ($resources as $key => $resource)
                                    <tr>
                                        <td>
                                            <a target="_blank" href="{{ isset($resource->filepath) ? asset($resource->filepath) : $resource->temporaryUrl() }}"
                                               class="card-link text-success"><i class="fas fa-file"></i>
                                                {{ isset($resource->filepath) ? $resource->filename : $resource->getClientOriginalName()  }}</a>
                                        </td>
                                        <td>
                                            <a wire:click="destroyLesson()" href="#"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div wire:ignore class="col-sm-12">
                        <label for="mytextarea">Descripción:</label>
                        <textarea id="mytextarea" wire:model="lesson_description" name="description">
                            {{ $lesson_description ?? '' }}
                        </textarea>
                        @error('title')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                <button type="submit" class="btn btn-success">{{ $lessonSelected ? 'Guardar cambios' : 'Agregar lección'}}</button>
                <button type="button" class="btn btn-light" data-dismiss="modal">Cerrar
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">


            </div>
        </div>
    </div>
</div>


