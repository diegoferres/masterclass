<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\CoursesController;
use App\Http\Controllers\Admin\LessonsController;
use App\Http\Controllers\Admin\StudentsController;
use App\Http\Controllers\Admin\CommentsController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Admin\OrdersController;

use App\Http\Controllers\HomeController as HomeControllerAlias;
use App\Http\Controllers\Web\CoursesController as CoursesControllerAlias;
use App\Http\Controllers\Web\OrdersController as OrdersControllerAlias;
use App\Http\Controllers\Web\UsersController as UsersControllerAlias;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\CategoriesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeControllerAlias::class, 'index'])->name('home');
Route::get('/mailview', function (){
    $order = \App\Models\Orders::first();
    return view('mail.buy-notification', compact('order'));
});

/*
 * Course section
 */
Route::get('/course/{slug}', [CoursesControllerAlias::class, 'course'])->name('course.show');
Route::get('/course/{course}/lesson/{lesson}', [CoursesControllerAlias::class, 'lesson'])->name('course.lesson')->middleware('auth');
Route::get('/lesson/{lesson}/finish', [CoursesControllerAlias::class, 'lessonFinish'])->name('course.lesson.finish')->middleware('auth');;
Route::get('/course/curso-diego/module/1/evaluation', function (){
    $eval  = \App\Models\Evaluations::first();
    return view('front.evaluation', compact('eval'));
})->name('course.module.evaluation')->middleware('auth');;


/*
 * Profile section
 */
Route::get('/profile', [UsersControllerAlias::class, 'index'])->name('profile');
Route::post('/profile/{id}/update', [UsersControllerAlias::class, 'update'])->name('profile.update');

/*
 * Payment section
 */
Route::get('/checkout/{course}', [OrdersControllerAlias::class, 'checkout'])->name('order.checkout')->middleware('auth');
Route::get('/checkout/{course}/finish', [OrdersControllerAlias::class, 'finish'])->name('order.checkout.finish');
Route::get('/payment/insert/{course}', [OrdersControllerAlias::class, 'insertPayment'])->name('order.payment.insert');
Route::post('/payment/confirm', [OrdersControllerAlias::class, 'confirmPayment'])->name('order.payment.confirm');
Route::get('/payment/rollback/{order}', [OrdersControllerAlias::class, 'rollbackPayment'])->name('order.payment.rollback');
Route::get('/payment/get/{order}', [OrdersControllerAlias::class, 'getPayment'])->name('order.payment.get');

Route::get('/payment/test', function (){
    $order = \App\Models\Orders::first();
    $course = $order->course;
    return view('front.store.finish', compact('order', 'course'));
})->name('order.payment.get');



Route::get('/miscursos', function(){
    return view('front.profile.miscursos');
});


//Route::middleware(['auth:sanctum', 'verified'])->group()->get('/dashboard', function () {
//    return view('dashboard');
//})->name('dashboard');


Route::group([
    'prefix' => 'admin',
    'as' => 'admin.',
    'middleware' => 'auth:sanctum'
    //'middleware' => ['auth:sanctum', 'role:admin']
], function () {
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

    Route::resource('categories', CategoriesController::class);
    Route::resource('courses', CoursesController::class);
    Route::resource('students', StudentsController::class);
    Route::resource('lessons', LessonsController::class);
    Route::resource('comments', CommentsController::class);
    Route::resource('users', UsersController::class);
    Route::resource('orders', OrdersController::class);

    //Route::resource('courses', )
});

Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
    //Route::resource('categories', CategoriesController::class);
});
