<?php

namespace App\Services;

use App\Models\Orders;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;


class BancardService
{
    public $bancardPublicKey,
        $bancardPrivateKey,
        $bancardRegisterNewCard,
        $bancardCharge,
        $bancardSingleBuy,
        $client,
        $returnUrl;

    public function __construct()
    {
        session_start();

        $environment = config('bancard.bancard_vpos_env'); // Obtenemos el entorno actual

        // Cargar claves y URLs dependiendo del entorno
        if ($environment === 'PROD') {
            $this->bancardPrivateKey = config('bancard.bancard_vpos_private_key_PROD');
            $this->bancardPublicKey = config('bancard.bancard_vpos_public_key_PROD');
            $this->bancardRegisterNewCard = config('bancard.bancard_vpos_service_url_PROD') . '/cards/new';
            $this->bancardCharge = config('bancard.bancard_vpos_service_url_PROD') . '/charge';
            $this->bancardSingleBuy = config('bancard.bancard_vpos_service_url_PROD') . 'vpos/api/0.3/single_buy';
        } else {
            // Configuración para entorno de desarrollo (DEV)
            $this->bancardPrivateKey = config('bancard.bancard_vpos_private_key_DEV');
            $this->bancardPublicKey = config('bancard.bancard_vpos_public_key_DEV');
            $this->bancardRegisterNewCard = config('bancard.bancard_vpos_service_url_DEV') . 'vpos/api/0.3/cards/new';
            $this->bancardCharge = config('bancard.bancard_vpos_service_url_DEV') . '/charge';
            $this->bancardSingleBuy = config('bancard.bancard_vpos_service_url_DEV') . 'vpos/api/0.3/single_buy';
        }

        $this->client = new Client([
            'base_uri' => $this->bancardSingleBuy, // Se puede ajustar según necesidad
            'verify' => false,
            'timeout' => 60,
            'allow_redirects' => true
        ]);

        $this->returnUrl = config('app.url'); // Usar la URL de la aplicación
    }

    /** SECCIÓN PAGO CON TOKEN **/
    /*
     * Get user cards
     */
    public function retrieveCards($user)

    {

        $string = $this->bancardPrivateKey;

        $string .= $user->id;

        $string .= 'request_user_cards';

        $requestCards = [

            'public_key' => $this->bancardPublicKey,

            'operation' => [

                'token' => md5($string)

            ]

        ];


        $requestCardsJson = json_encode($requestCards);


        try {

            $url = "https://vpos.infonet.com.py:8888/vpos/api/0.3/users/{$user->id}/cards";

//            $url = "https://vpos.infonet.com.py/vpos/api/0.3/users/{$user->id}/cards";


            $response = $this->client->request('POST', $url,

                ['body' => $requestCardsJson, 'verify' => false, 'timeout' => 60, 'allow_redirects' => true]);


            if ($response->getStatusCode() == 200) {

                $bancardJsonResponse = $response->getBody()->getContents();

                $bancardResponse = json_decode($bancardJsonResponse);

                if ($bancardResponse->status == 'success') {

                    foreach ($bancardResponse->cards as $card) {

                        /*

                         * Define la tarjeta por defecto para el débito automático

                         * SI es 1, se asigna automáticamente como vigente

                         */

                        if (count($bancardResponse->cards) == 1) {

                            $validity = true;

                        } else {

                            $validity = false;

                        }


                        $cardsToSave = [

                            'user_id' => $user->id,

                            'card_token' => $card->alias_token,

                            'card_number' => $card->card_masked_number,

                            'card_expiration' => $card->expiration_date,

                            'card_brand' => $card->card_brand,

                            'card_type' => $card->card_type,

                            'validity' => $validity,

                            'bancard_id' => $card->card_id

                        ];


                        // Buscamos la tarjeta por el card_id (ID de la BD Local)

                        if ($currentCard = Cards::where('bancard_id', '=', $card->card_id)->first()) {

                            $currentCard->update($cardsToSave);

                        } else {

                            Cards::create($cardsToSave);

                        }

                    }

                    // Eliminamos las tarjetas que no tengan un token de sesion (esto usamos como bandera)

                    Cards::where('card_token', '=', null)->where('user_id', '=', $user->id)->delete();


                    $cards = Cards::orderBy('id', 'desc')->get();

                    return true;

                }

            }

        } catch (\Exception $e) {

            \Log::warning("Problemas de conexión con bancard " . $e->getMessage());

            return false;

        }

    }

    /*
     * Add new card
     */
    public function addCards($user, $request = null)

    {

        if (array_key_exists(0, $user->cards->toArray())) {

            $newCard = $user->cards[0];

        } else {

            $newCard = Cards::create(

                [

                    'user_id' => $user->id,

                    'card_number' => 1,

                    'card_brand' => 'N',

                    'card_type' => 'N',

                    'card_expiration' => 'N',

                    'bancard_id' => 0

                ]

            );

        }

        $flag = explode('=', $request);

        if ($flag[0] == 'gift_id') {

            //$url = 'https://galeriamatices.com.py/giftcard/buy?' . $request;

            $url = 'https://galeriamatices.com.py/giftcard/buy?' . $request;

        } elseif (!is_null($request)) {

            //$url = "https://galeriamatices.com.py/carrito/checkout/pay_method?" . $request;

            $url = "https://galeriamatices.com.py/carrito/checkout/pay_method?" . $request;

        } else {

            //$url = "https://galeriamatices.com.py/carrito/";

            $url = "https://galeriamatices.com.py/carrito/";

        }

        \Log::info($url);

        $string = $this->bancardPrivateKey;

        $string .= $newCard->id;

        $string .= $user->id;

        $string .= 'request_new_card';

        $registerNewCard = [

            'public_key' => $this->bancardPublicKey,

            'operation' => [

                'token' => md5($string),

                'card_id' => $newCard->id,

                'user_id' => $user->id,

                'user_cell_phone' => $user->telephone,

                'user_mail' => $user->email,

                'return_url' => $url

            ],

        ];


        $registerNewCardJson = json_encode($registerNewCard);


        try {

            $response = $this->client->request(

                'POST',

                $this->bancardRegisterNewCard,

                ['body' => $registerNewCardJson, 'verify' => false, 'timeout' => 60, 'allow_redirects' => true]

            );


            if ($response->getStatusCode() == 200) {

                $bancardJsonResponse = $response->getBody()->getContents();

                \Log::info("CartController | Bancard response: {$bancardJsonResponse}");

                $bancardResponse = json_decode($bancardJsonResponse);

                if ($bancardResponse->status == 'success') {

                    return $bancardResponse->process_id;

                } else {

                    \Log::warning('CartController | Bancard returns a error message');

                    return false;

                }

            } else {

                \Log::warning('CartController | Bancard returns a error code message');

                return false;

            }

        } catch (\Exception $e) {

            \Log::warning("CartController | Error attempting to call register new card API");

            \Log::warning($e->getMessage());

            Cards::destroy($newCard->id);

            return false;

        }

    }

    /*
     * Destroy user card
     */
    public function destroyACard($user, $card_id)

    {

        if ($card = Cards::find($card_id)) {

            $string = $this->bancardPrivateKey;

            $string .= "delete_card";

            $string .= $user->id;

            $string .= $card->card_token;


            $deleteCard = [

                'public_key' => $this->bancardPublicKey,

                'operation' => [

                    'token' => md5($string),

                    'alias_token' => $card->card_token

                ]

            ];


//            $apiURL = "https://vpos.infonet.com.py/vpos/api/0.3/users/{$user->id}/cards/";

            $apiURL = "https://vpos.infonet.com.py:8888/vpos/api/0.3/users/{$user->id}/cards/";

            $deleteCardJson = json_encode($deleteCard);

            if (!is_null($card->card_token)) {

                try {

                    $response = $this->client->request('DELETE', $apiURL,

                        ['body' => $deleteCardJson, 'verify' => false, 'timeout' => 60, 'allow_redirects' => true]);

                    if ($response->getStatusCode() == 200) {

                        $bancardJsonResponse = $response->getBody()->getContents();

                        $bancardResponse = json_decode($bancardJsonResponse);

                        if ($bancardResponse->status == 'success') {

                            if ($card->delete()) {

                                \Log::info("CartController | Card deleted successfully");

                                return true;

                            } else {

                                \Log::warning("CartController | Error attempting to delete the given card - {$card->id}");

                                return false;

                            }

                        }

                    }

                } catch (\Exception $e) {

                    \Log::warning($e->getMessage());

                    return false;

                }

            }

        }

        return false;

    }

    /*
     * Execute bancard payment with user card saved
     */
    public function makePayment($user, $invoice)
    {
        $this->retrieveCards($user);

        $string = $this->bancardPrivateKey;

        $string .= $invoice->id;

        $string .= "charge";

        $string .= number_format($invoice->total_amount, 2, '.', '');

        $string .= "PYG";

        $string .= $user->cards[0]->card_token;


        $totalPrice = $invoice->total_amount;

        if ($invoice->total_discounts != 0) {

            $totalPrice = $totalPrice - $discounts;

        }

        $sendPayment = [

            'public_key' => $this->bancardPublicKey,

            'operation' => [

                'token' => md5($string),

                'shop_process_id' => $invoice->id,

                'amount' => number_format($totalPrice, 2, '.', ''),

                'number_of_payments' => 1,

                'currency' => "PYG",

                'additional_data' => "",

                'description' => 'Pago - Matices Web',

                'alias_token' => $user->cards[0]->card_token

            ],

        ];


        $sendPaymentJson = json_encode($sendPayment);

        \Log::debug('CartController | Sending new payment to bancard ' . $sendPaymentJson);


        try {

            $response = $this->client->request(

                'POST',

                $this->bancardCharge,

                ['body' => $sendPaymentJson, 'verify' => false, 'timeout' => 60, 'allow_redirects' => true]

            );


            if ($response->getStatusCode() == 200) {

                $bancardJsonResponse = $response->getBody()->getContents();

                $bancardResponse = json_decode($bancardJsonResponse);


                if ($bancardResponse->status == 'success') {

                    \Log::debug("CartController | SendPayment response " . $bancardJsonResponse);

                    if ($bancardResponse->confirmation->response_code != 00 || $bancardResponse->confirmation->response_code == null) {

                        $invoice->bancard_code_error = $bancardResponse->confirmation->response_code;

                        $invoice->bancard_message = $bancardResponse->confirmation->response_description;

                        $invoice->save();

                        return false;

                    }

                    $invoice->bancard_ticket_number = $bancardResponse->confirmation->ticket_number;

                    $invoice->bancard_code_error = $bancardResponse->confirmation->response_code;

                    $invoice->bancard_message = $bancardResponse->confirmation->extended_response_description;


                    // Guardamos los datos de la tarjeta

                    $invoice->bancard_card_number = $user->cards[0]->card_number;

                    $invoice->bancard_card_name = $user->cards[0]->card_brand;


                    $invoice->save();


                    return true;

                }

            }

        } catch (\Exception $e) {

            \Log::warning("CartController | Error attempting to call to charge card API");

            \Log::warning($e->getMessage());

            return false;

        }

        return false;

    }

    /** PAGO OCASIONAL **/
    /*
     * Insert bancard payment
     */
    public function singleBuy($order)
    {
        try {

            \DB::beginTransaction();
            $amount = number_format($order->amount, 2, '.', '');
            $string = $this->bancardPrivateKey;
            $string .= $order->id;
            $string .= $amount;
            $string .= 'PYG';

            $singlebuy = [
                'public_key' => $this->bancardPublicKey,
                'operation' => [
                    'token' => md5($string),
                    'shop_process_id' => $order->id,
                    'currency' => 'PYG',
                    'amount' => $amount,
                    'aditional_data' => '',
                    'description' => 'Masquelier | Curso ' . $order->course->title,
                    'return_url' => $this->returnUrl.'/checkout/' . $order->id . '/finish',
                    'cancel_url' => $this->returnUrl.'/checkout/' . $order->course->slug,
                ],
            ];

            $response = $this->client->request(
                'POST',
                '',
                [
                    'json' => $singlebuy,
                ]
            )->getBody()->getContents();


            \DB::commit();

            \Log::debug('BancardService@sigleBuy response - '.$response);

            return json_decode($response);

        }catch (ClientException $e){
            \DB::rollBack();
            \Log::error('BancardService@singleBuy response - Line: '.$e->getLine() .' Message: '.$e->getMessage());

            return json_decode('{"status":"error"}');
        }
    }

    /*
     * Insert bancard payment
     */
    public function rollbackPayment($order)
    {
        $string = $this->bancardPrivateKey;
        $string .= $order->id;
        $string .= 'rollback0.00';
        $singleRollBack = [
            'public_key' => $this->bancardPublicKey,
            'operation' => [
                'token' => md5($string),
                'shop_process_id' => $order->id,
            ],
        ];

        $response = $this->client->request(
            'POST',
            'single_buy/rollback',
            [
                'json' => $singleRollBack,
            ]
        );

        if ($response->getBody()->getContents()->status == 'success') {
            return $response->getBody()->getContents();
        } else {
            return false;
        }
    }

    public function getPayment($order)
    {
        $string = $this->bancardPrivateKey;
        $string .= $order->id;
        $string .= 'get_confirmation';
        $getconfirmation = [
            'public_key' => $this->bancardPublicKey,
            'operation' => [
                'token' => md5($string),
                'shop_process_id' => $order->id,
            ],
        ];

        $response = $this->client->request(
            'POST',
            'single_buy/confirmations',
            [
                'json' => $getconfirmation,

            ]
        );

        return $response->getBody()->getContents();

    }
}

