<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modules extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'resource_path',
        'sort',
        'available',
        'course_id',
    ];

    public function course()
    {
        return $this->belongsTo(Courses::class, 'course_id');
    }

    public function lessons()
    {
        return $this->hasMany(Lessons::class, 'module_id');
    }

    public function evaluation()
    {
        return $this->hasOne(Evaluations::class, 'module_id');
    }

    public function previous()
    {
        return $this->where('sort','<', $this->sort)->where('course_id', $this->course_id)->orderBy('sort', 'DESC')->first();
    }

    public function next()
    {
        return $this->where('sort','>', $this->sort)->where('course_id', $this->course_id)->orderBy('sort', 'ASC')->first();
    }

    public function progress()
    {
        if ($this->lessons()->whereHas('userLesson')->count() > 0) {
            //$progress = (($this->lessons()->whereHas('userLesson')->count() + (isset($this->evaluation->currentUserEvaluation) ? 1 : 0)) * 100) / $this->lessons->count() + ($this->evaluation ? 1 : 0);
            $progress = (($this->lessons()->whereHas('userLesson')->count()) * 100) / $this->lessons->count() + ($this->evaluation ? 1 : 0);
        }else{
            $progress = 0;
        }

        return  $progress;
    }
}
