<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Courses extends Model
{
    use HasFactory, HasSlug;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'intro_resource',
        'price',
        'datetime_start',
        'datetime_end',
        'price',
        'available',
        'slug',
    ];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    /*public function categories()
    {
        return $this->morphToMany(Categories::class, 'category');
    }*/

    public function categories()
    {
        return $this->belongsToMany(Categories::class, 'course_category', 'course_id', 'category_id');
    }

    public function modules()
    {
        return $this->hasMany(Modules::class, 'course_id');
    }

    public function lessons()
    {
        return $this->hasManyThrough(Lessons::class, Modules::class, 'course_id', 'module_id');
    }

    public function orders()
    {
        return $this->hasMany(Orders::class, 'course_id');
    }

    public function students()
    {
        return $this->belongsToMany(User::class, 'orders', 'user_id', 'course_id');
    }
}
