<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Lessons extends Model
{
    use HasFactory, HasSlug;

    protected $fillable = [
        'title',
        'description',
        'resource_path',
        'module_id',
        'slug'
    ];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    public function module()
    {
        return $this->belongsTo(Modules::class, 'module_id');
    }

    public function files()
    {
        return $this->hasMany(LessonFiles::class, 'lesson_id');
    }

    public function comments()
    {
        return $this->hasMany(Comments::class, 'lesson_id');
    }

    public function userLesson()
    {
        return $this->hasOne(UserLessons::class, 'lesson_id')->where('user_id', \Auth::id());
    }

    public function previous()
    {
        return $this->where('sort','<', $this->sort)->where('module_id', $this->module_id)->orderBy('sort', 'DESC')->first();
    }

    public function next()
    {
        return $this->where('sort','>', $this->sort)->where('module_id', $this->module_id)->orderBy('sort', 'ASC')->first();
    }
}
