<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $fillable = [
        'description'
    ];

    public function courses()
    {
        return $this->belongsToMany(Courses::class, 'course_category', 'course_id', 'category_id');
    }
}
