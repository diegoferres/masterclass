<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsersEvalOptions extends Model
{
    use HasFactory;

    protected $fillable = [
        'option_id',
        'users_evaluation_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function option()
    {
        return $this->belongsTo(Options::class, 'option_id');
    }
}
