<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
    use HasFactory;

    protected $fillable = [
        'evaluation_id',
        'text',
        'points',
        'active',
    ];

    public function options()
    {
        return $this->hasMany(Options::class, 'item_id');
    }
}
