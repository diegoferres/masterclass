<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Evaluations extends Model
{
    use HasFactory;

    protected $fillable = [
        'module_id',
        'active'
    ];

    public function module()
    {
        return $this->belongsTo(Modules::class, 'module_id');
    }

    public function items()
    {
        return $this->hasMany(Items::class, 'evaluation_id');
    }

    public function currentUserEvaluation()
    {
        return $this->hasOne(UserEvaluations::class, 'evaluation_id')->where('user_id', \Auth::id());
    }

    public function usersEvaluations()
    {
        return $this->hasMany(UserEvaluations::class, 'evaluation_id');
    }
}
