<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Options extends Model
{
    use HasFactory;

    protected $fillable = [
        'item_id',
        'text',
        'is_answer',
        'active',
    ];

    public function item()
    {
        return $this->belongsTo(Items::class, 'item_id');
    }
}
