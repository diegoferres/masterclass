<?php

namespace App\Http\Livewire\Admin\Comments;

use App\Models\Comments;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $search = '';
    public $reply;
    public $reply_comment;

    public function saveComment(Comments $comment)
    {
        Comments::create([
           'comment' => $this->reply_comment,
           'parent' => $comment->id,
           'lesson_id' => $comment->lesson_id,
           'user_id' => \Auth::id(),
        ]);

        $this->reply = null;
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.admin.comments.index', [
            'comments' => Comments::where(function ($w){
              $w->whereHas('lesson.module.course', function ($q) {
                  $q->whereRaw("LOWER(title) like '%" . strtolower($this->search) . "%'");
              })->orWhereHas('user', function ($r) {
                  $r->whereRaw("LOWER(name) like '%" . strtolower($this->search) . "%'");
              });
            })->where('parent', null)
                ->orderBy('created_at', 'DESC')->paginate(5),
        ]);
    }
}
