<?php

namespace App\Http\Livewire\Admin\Users;

use App\Models\Comments;
use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    public $search;

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.admin.users.index', [
            'users' => User::whereRaw("LOWER(email) like '%" . strtolower($this->search) . "%'")
                ->orWhereRaw("LOWER(name) like '%" . strtolower($this->search) . "%'")
                ->orderBy('created_at', 'DESC')->paginate(5),
        ]);
    }
}
