<?php

namespace App\Http\Livewire\Admin\Students;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $search = '';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.admin.students.index', [
            'students' => User::whereRaw("LOWER(name) like '%" . strtolower($this->search) . "%'")
                ->whereHas('roles', function ($q) {
                    $q->where('name', 'student');
                })->whereHas('orders', function ($q) {
                    $q->where('paid', true);
                })->paginate(10),
        ]);
    }
}
