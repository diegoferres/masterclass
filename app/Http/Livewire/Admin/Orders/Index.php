<?php

namespace App\Http\Livewire\Admin\Orders;

use App\Models\Orders;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $search = '';

    public function paid(Orders $order)
    {
        $order->paid = !$order->paid;
        $order->save();
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.admin.orders.index', [
            /*'orders' => Orders::orderBy('created_at', 'DESC')->paginate(5),*/
            'orders' => Orders::whereHas('user', function ($r) {
                $r->where('name', 'like', '%'.$this->search.'%')
                    ->orWhere('email', 'like', '%'.$this->search.'%');
            })->orderBy('created_at', 'DESC')->paginate(5),
        ]);
        //return view('livewire.admin.orders.index');
    }
}
