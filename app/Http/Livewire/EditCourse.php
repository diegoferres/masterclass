<?php

namespace App\Http\Livewire;

use App\Models\Categories;
use App\Models\CourseCategory;
use App\Models\Courses;
use App\Models\Evaluations;
use App\Models\Items;
use App\Models\LessonFiles;
use App\Models\Lessons;
use App\Models\Modules;
use App\Models\Options;
use Illuminate\Database\Eloquent\Model;
use Livewire\Component;
use Livewire\WithFileUploads;

class EditCourse extends Component
{
    use WithFileUploads;

    public $course;
    public $course_title;
    public $course_resourceNew;
    public $course_intro_resource;
    public $course_description;

    public $modules;

    /* Course Options */
    public $categories;
    public $course_datetime_start;
    public $course_datetime_end;
    public $course_price;

    /* Modules */
    public $moduleSelected;
    public $moduleTitle;

    /* Lesson */
    public $lessonOpen = false;
    public $lesson;
    public $lessonSelected;
    public $lesson_title;
    public $lesson_description;
    public $lesson_resource;
    public $resources = [];
    public $img_resource = [];
    public $confirmDeleteLesson = false;

    /* Evaluation */
    public $evaluationOpen = false;
    public $evalItems = [];
    public $evalOptions = [];
    public $evalItemTitle;
    public $evalItemPoints;
    public $iteration;
    public $evaluationSelected;

    /*
     * Modules section
     */
    public function addModule()
    {
        if (strlen(trim($this->moduleTitle)) == 0) {
            $this->dispatchBrowserEvent('alert', ['type' => 'warning', 'message' => 'El nombre del módulo es requerido']);
            $this->moduleTitle = null;
            return false;
        }

        $sort = Modules::where('course_id', $this->course->id)->orderBy('sort', 'ASC')->get()->last();

        //dd($sort ? $sort->sort + 1 : 1);
        (new Modules())->create([
            'title' => $this->moduleTitle,
            'course_id' => $this->course->id,
            'sort' => ($sort->sort ?? 0) + 1
        ]);

        $this->moduleTitle = null;

        $this->course = Courses::find($this->course->id);
    }

    public function sortModules($sort)
    {
        foreach ($sort as $section) {

            $b = Modules::find($section['value']);
            $b->sort = $section['order'];
            $b->save();
        }

        $this->course = Courses::find($this->course->id);
    }

    public function selectModule(Modules $module)
    {
        $this->moduleSelected = $module;
    }

    public function deleteModule($id)
    {
        Modules::destroy($id);

        $this->course = Courses::find($this->course->id);
    }

    /*
     * Lesson section
     */
    public function addLesson()
    {
        $this->lessonOpen = true;
        $this->unselectLesson();
        $this->dispatchBrowserEvent('GoToElement', 'lesson_form');
    }

    public function createLesson()
    {
        try {
            \DB::beginTransaction();
            $lesson = new Lessons();
            $lesson->title = $this->lesson_title;
            $lesson->description = $this->lesson_description;
            $lesson->module_id = $this->moduleSelected->id;

            $ls = Lessons::where('module_id', $this->moduleSelected->id)->orderByDesc('sort')->first();
            $lesson->sort = $ls ? $ls->sort + 1 : 0 + 1;

            $lesson->save();

            if ($this->lesson_resource) {
                $filename = basename($this->lesson_resource->store('public/courses/' . $this->course->id . '/lessons/' . $lesson->id));
                $lesson->resource_path = 'courses/' . $this->course->id . '/lessons/' . $lesson->id . '/' . $filename;
                $lesson->save();
            }


            if ($this->resources) {
                foreach ($this->resources as $resource) {
                    $resourceFile = new LessonFiles();
                    $filename = basename($resource->store('public/courses/' . $this->course->id . '/lessons/' . $lesson->id . '/resources/'));
                    $resourceFile->filepath = 'courses/' . $this->course->id . '/lessons/' . $lesson->id . '/resources/' . $filename;
                    $resourceFile->filename = $resource->getClientOriginalName();
                    $resourceFile->lesson_id = $lesson->id;
                    $resourceFile->save();
                }
            }

            \DB::commit();

            $this->moduleSelected = Modules::find($this->moduleSelected->id);

            $this->dispatchBrowserEvent('alert', ['type' => 'success', 'message' => 'Lección creada']);
        } catch (\Exception $e) {
            \DB::rollBack();
            \Log::error('livewire/EditCourse@createLesson: ' . $e->getLine() . ' - ' . $e->getMessage());

            $this->dispatchBrowserEvent('alert', ['type' => 'error', 'message' => 'No se pudo crear. Consulte el administrador']);
        }
    }

    public function updateLesson()
    {
        try {
            \DB::beginTransaction();
            $this->lessonSelected->title = $this->lesson_title;
            $this->lessonSelected->description = $this->lesson_description;
            $this->lessonSelected->module_id = $this->moduleSelected->id;
            //$lesson->sort           = Lessons::where('module_id', $this->moduleSelected->id)->orderByDesc('sort')->first()->sort + 1;;
            $this->lessonSelected->save();

            if ($this->lesson_resource) {
                if (\Storage::exists('public/' . $this->lessonSelected->resource_path)) {
                    \Storage::delete('public/' . $this->lessonSelected->resource_path);
                }

                $filename = basename($this->lesson_resource->store('public/courses/' . $this->course->id . '/lessons/' . $this->lessonSelected->id));
                $this->lessonSelected->resource_path = 'courses/' . $this->course->id . '/lessons/' . $this->lessonSelected->id . '/' . $filename;
                $this->lessonSelected->save();

                $this->lesson_resource = null;
            }

            if ($this->resources) {
                foreach ($this->resources as $resource) {
                    $resourceFile = new LessonFiles();
                    $filename = basename($resource->store('public/courses/' . $this->course->id . '/lessons/' . $this->lessonSelected->id . '/resources/'));
                    $resourceFile->filepath = 'courses/' . $this->course->id . '/lessons/' . $this->lessonSelected->id . '/resources/' . $filename;
                    $resourceFile->filename = $resource->getClientOriginalName();
                    $resourceFile->lesson_id = $this->lessonSelected->id;
                    $resourceFile->save();
                }
                $this->resources = null;
            }

            \DB::commit();

            $this->lessonSelected = Lessons::find($this->lessonSelected->id);
            $this->lesson_resource = null;
            $this->resources = null;

            $this->dispatchBrowserEvent('alert', ['type' => 'success', 'message' => 'Lección actualizada']);
        } catch (\Exception $e) {
            \DB::rollBack();
            \Log::error('livewire/EditCourse@updateLesson: ' . $e->getLine() . ' - ' . $e->getMessage());

            $this->dispatchBrowserEvent('alert', ['type' => 'error', 'message' => 'No se pudo actualizar. Consulte el administrador']);
        }
    }

    public function sortLessons($sort)
    {
        foreach ($sort as $lesson) {

            $b = Lessons::find($lesson['value']);
            $b->sort = $lesson['order'];
            $b->save();
        }

        //$this->course = Courses::find($this->course->id);
    }

    public function deleteLessonResource()
    {
        $this->lesson_resource = null;
    }

    public function deleteLessonFiles($id = false, $new = false)
    {

        //dd(\Storage::delete('public/'.$lesson->filepath));
        if ($id) {
            if ($new) {
                unset($this->resources[$id]);
            } else {
                $lesson = LessonFiles::find($id);
                if (\Storage::exists('public/' . $lesson->filepath)) {
                    \Storage::delete('public/' . $lesson->filepath);
                }

                LessonFiles::destroy($id);
            }
        } else {
            unset($this->resources[0]);
        }

        $this->lessonSelected = Lessons::find($this->lessonSelected->id);
    }

    public function selectLesson(Lessons $lesson)
    {
        $this->lessonSelected = $lesson;
        $this->lesson_title = $lesson->title;
        //$this->lesson_resource = $lesson->resource_path;
        //$this->resources = $lesson->files;
        $this->lesson_description = $lesson->description;

        $this->dispatchBrowserEvent('editLesson', $lesson->description);
        //$this->lesson_description = $lesson;
        $this->dispatchBrowserEvent('GoToElement', 'lesson_form');
        $this->lessonOpen = true;
    }

    public function unselectLesson()
    {
        $this->lessonSelected = null;
        $this->lesson_title = null;
        $this->lesson_description = null;
        $this->resources = null;
        $this->lesson_resource = null;
        $this->dispatchBrowserEvent('editLesson', null);
    }

    public function editLesson()
    {
        $this->dispatchBrowserEvent('editLesson');
    }

    public function deleteLesson()
    {
        $lesson = $this->lessonSelected;

        /* Delete lesson files from storage */
        if (\Storage::exists('public/courses/' . $lesson->module->course_id . '/lessons/' . $lesson->id)) {
            \Storage::deleteDirectory('public/courses/' . $lesson->module->course_id . '/lessons/' . $lesson->id);
        }
        $lesson->delete();

        $this->confirmDeleteLesson = false;
        $this->unselectLesson();
        $this->moduleSelected = Modules::find($this->moduleSelected->id);

        $this->dispatchBrowserEvent('alert', ['type' => 'success', 'message' => 'Curso eliminado']);
    }

    /*
     * Evaluation section
     */
    public function addEvaluation()
    {
        //dd('addEvaluation');
        $this->lessonOpen = false;
        $this->evaluationOpen = true;
        $this->evaluationSelected = null;
        $this->evalItems = [];
        $this->dispatchBrowserEvent('GoToElement', 'evaluation_form');
    }

    /* Add item to module evaluation */
    public function addEvalItem()
    {
        $this->validate([
            'evalItemTitle' => 'required',
        ]);

        $this->evalItems[] = ['title' => $this->evalItemTitle, 'points' => $this->evalItemPoints];
        $this->evalItemTitle = null;
        $this->evalItemPoints = null;
    }

    /* Remove item from module evaluation */
    public function deleteItem($itemKey)
    {
        //dd($itemKey);
        try {
            \DB::beginTransaction();

            if ($this->evaluationSelected) {
                Options::where('item_id', $itemKey)->delete();
                Items::find($itemKey)->delete();
            }

            \DB::commit();
        }catch (\Exception $e) {
            \DB::rollBack();
            \Log::error('livewire/EditCourse@deleteItem: ' . $e->getLine() . ' - ' . $e->getMessage());
        }


        unset($this->evalItems[$itemKey]);
    }

    /* Add option to specific item */
    public function addItemOption($key)
    {
        $this->evalItems[$key]['options'][] = ['answer' => false, 'title' => null];
    }

    /* Uncheck all options of specific items */
    public function uncheckAllItemOptions($itemKey, $optKey)
    {
        foreach ($this->evalItems[$itemKey]['options'] as $key => $opt) {
            if ($key != $optKey) {
                $this->evalItems[$itemKey]['options'][$key]['answer'] = false;
            }
        }
    }

    /* Remove option from specific item */
    public function deleteItemOption($itemKey, $optionKey)
    {
        if ($this->evaluationSelected) {
            Options::find($optionKey)->delete();
        }

        unset($this->evalItems[$itemKey]['options'][$optionKey]);
    }

    public function saveEvaluation()
    {

        try {
            \DB::beginTransaction();

            if ($this->evaluationSelected) {
                $eval = $this->evaluationSelected;
            }else{
                $eval = new Evaluations();
                $eval->module_id = $this->moduleSelected->id;
                $eval->save();
            }

            //dd($this->evalItems);
            foreach ($this->evalItems as $item) {
                //dd($item);
                if (array_key_exists('id',$item)) {
                    $it = Items::find($item['id']);
                }else{
                    $it = new Items();
                    $it->evaluation_id = $eval->id;
                }
                //dd($it);
                //$it = new Items();

                $it->text = $item['title'];
                $it->points = $item['points'];
                $it->save();
                //dd($it);


                if (!$item['options'] || count($item['options']) < 2) {
                    $this->dispatchBrowserEvent('alert', ['type' => 'error', 'message' => 'Se necesita al menos dos respuestas por cada item de evaluación']);
                    return;
                }

                if (!array_search(true, array_column($item['options'], 'answer'))) {
                    $this->dispatchBrowserEvent('alert', ['type' => 'error', 'message' => 'Se necesita al menos una respuesta correcta por cada item de evaluación']);
                    return;
                }
                foreach ($item['options'] as $option) {
                    if (array_key_exists('id',$option)) {
                        $opt = Options::find($option['id']);
                    }else{
                        $opt = new Options();
                        $opt->item_id = $it->id;
                    }
                    $opt->text = $option['title'];
                    $opt->is_answer = $option['answer'];
                    $opt->save();
                }
            }

            \DB::commit();

            $this->moduleSelected = Modules::find($this->moduleSelected->id);

            $this->dispatchBrowserEvent('alert', ['type' => 'success', 'message' => 'Evaluación guardada correctamente']);
        } catch (\Exception $e) {
            dd($e);
            \DB::rollBack();
            \Log::error('livewire/EditCourse@saveEvaluation: ' . $e->getLine() . ' - ' . $e->getMessage());

            $this->dispatchBrowserEvent('alert', ['type' => 'error', 'message' => 'Ocurrió un error al guardar la evaluación. Contacte con el administrador']);
        }
    }

    public function selectEvaluation(Evaluations $evaluation)
    {
        foreach ($evaluation->items as $item) {
            $this->evalItems[$item->id] = [
                'id' => $item->id,
                'title' => $item->text,
                'points' => $item->points,
                'exists' => true,
                'options' => []
            ];

            foreach ($item->options as $option) {
                $this->evalItems[$item->id]['options'][$option->id] = [
                    'id' => $option->id,
                    'title' => $option->text,
                    'answer' => $option->is_answer,
                ];
            }
        }
        //dd($evaluation->items);
        //$this->evaluationSelected = $evaluation;
        $this->evaluationOpen = true;
        $this->evaluationSelected = $evaluation;
        //$this->evaluationSelectedOpen = true;
    }

    public function mount()
    {
        $this->course_title = $this->course->title;
        $this->course_intro_resource = $this->course->intro_resource;
        $this->course_description = $this->course->description;

        $this->categories = Categories::pluck('description', 'id');
        $this->course_datetime_start = $this->course->datetime_start;
        $this->course_datetime_end = $this->course->datetime_end;
        $this->course_price = $this->course->price;

        /*
         * Evaluation
         */
        /*$this->evalItems = [
            [
                'title' => 'En un estudio para determinar los factores asociados a la presencia de accidentes biológicos en los trabajadores de salud de un hospital, se encontraron los resultados que muestra la tabla',
                'points' => 5,
                'options' => [
                    [
                        'answer' => true,
                        'title' => 'Ser hombre, tener más de 50 años de edad, no tener un accidente biológico previo y haber trabajado una jornada menor o igual que 8 horas antes del accidente',
                    ],
                    [
                        'answer' => false,
                        'title' => 'Según la Encuesta Nacional de Situación Nutricional (ENSIN) 2010, el 8 % de los niños entre 5 y 12 años de edad presentan anemia, y la población del área rural es la más afectada. En respuesta a esta situación, se decidió implementar un programa de suplementación con sulfato ferroso para los estudiantes de la escuela de una vereda. La mejor forma de evaluar el impacto de este programa es',
                    ],
                ],
            ],
            [
                'title' => 'En un estudio para determinar los factores asociados a la presencia de accidentes biológicos en los trabajadores de salud de un hospital, se encontraron los resultados que muestra la tabla',
                'points' => 5,
                'options' => [
                    [
                        'answer' => true,
                        'title' => 'Ser hombre, tener más de 50 años de edad, no tener un accidente biológico previo y haber trabajado una jornada menor o igual que 8 horas antes del accidente',
                    ],
                    [
                        'answer' => false,
                        'title' => 'Según la Encuesta Nacional de Situación Nutricional (ENSIN) 2010, el 8 % de los niños entre 5 y 12 años de edad presentan anemia, y la población del área rural es la más afectada. En respuesta a esta situación, se decidió implementar un programa de suplementación con sulfato ferroso para los estudiantes de la escuela de una vereda. La mejor forma de evaluar el impacto de este programa es',
                    ],
                ],
            ],
        ];*/
    }

    public function render()
    {
        return view('livewire.edit-course');
    }
}
