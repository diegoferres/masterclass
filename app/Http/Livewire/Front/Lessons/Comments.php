<?php

namespace App\Http\Livewire\Front\Lessons;

use App\Models\Comments as CommentsAlias;
use App\Models\Lessons;
use Livewire\Component;

class Comments extends Component
{
    public $lesson;
    public $commentInput;
    public $commentResponse;
    public $parentComment = false;

    public $commentOpen = false; //default closed status comment textarea

    public function replyComment(CommentsAlias $replyComment)
    {
        $this->commentResponse = $replyComment;
        $this->commentOpen = true;
    }

    public function cancelReply()
    {
        $this->commentResponse = null;
        $this->commentOpen = false;
    }

    public function saveComment()
    {
        CommentsAlias::create([
            'parent'    => $this->commentResponse ? $this->commentResponse->id : null,
            'lesson_id' => $this->lesson->id,
            'user_id'   => \Auth::id(),
            'comment'   => $this->commentInput,
        ]);

        $this->lesson = Lessons::find($this->lesson->id);

        $this->commentInput = null;
        $this->commentResponse = null;
        $this->commentOpen = false;
    }

    public function destroyComment($commentId)
    {
        CommentsAlias::destroy($commentId);
        $this->lesson = Lessons::find($this->lesson->id);
    }

    public function render()
    {
        return view('livewire.front.lessons.comments');
    }
}
