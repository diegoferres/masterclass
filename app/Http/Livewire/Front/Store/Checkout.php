<?php

namespace App\Http\Livewire\Front\Store;

use App\Models\Orders;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

class Checkout extends Component
{
    public $course;

    public $payment_method = 1, $voucher, $paymentMethods = [];

    public function finish()
    {
        $this->validate([
            'voucher' => 'required_if:payment_method,2'
        ],[],[
            'voucher' => 'nro. de comprobante'
        ]);
        if ($this->payment_method == 1) {
            return redirect()->route('order.payment.insert', $this->course->id);
        }else {
            try {
                \DB::beginTransaction();
                $order = new Orders();
                $order->amount              = $this->course->price;
                $order->paid                = false;
                $order->course_id           = $this->course->id;
                $order->user_id             = \Auth::id();
                $order->payment_method_id   = $this->payment_method;
                $order->voucher             = $this->voucher;
                $order->save();

                \DB::commit();

                return redirect()->route('order.checkout.finish', $order->id);

            }catch (\Exception $e){
                \DB::rollBack();
                Log::error('Livewire\Front\Store\Checkout@finish Line: '.$e->getLine().' - Message: '.$e->getMessage());
                return redirect()->route('order.checkout', $this->course->slug)->with('error', 'No se pudo procesar la orden de pago');
            }
        }
    }

    public function render()
    {
        $this->paymentMethods = \DB::table('payment_methods')->where('active', 1)->get();
        return view('livewire.front.store.checkout');
    }
}
