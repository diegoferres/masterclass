<?php

namespace App\Http\Livewire\Front\Course\Module;

use App\Models\Items;
use App\Models\Options;
use App\Models\UserEvaluations;
use App\Models\UsersEvalOptions;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Evaluation extends Component
{
    public $eval, $items = [], $evalEnded = false, $confirm = false, $points, $error = false;

    public function resetEvaluation()
    {
        UserEvaluations::where('user_id', Auth::id())->where('evaluation_id', $this->eval->id)->first()->delete();
        $this->evalEnded = false;
        $this->items = [];
        $this->points = 0;
        $this->confirm = false;
    }

    public function saveEvaluation()
    {
        try {
            \DB::beginTransaction();

            if (count($this->items) < Items::where('evaluation_id', $this->eval->id)->count()) {
                $this->evalEnded = false;
                $this->error = true;
                $this->dispatchBrowserEvent('GoToTop');
                return false;
            }

            /* Saving user evaluation points */
            $userEval = UserEvaluations::create([
                'evaluation_id' => $this->eval->id,
                'user_id' => \Auth::id(),
                'total_points' => 0,
            ]);

            $points = 0;
            foreach ($this->items as $item => $option) {
                \Log::info('option: '.$option);

                $opt = Options::where('id', $option)->first();

                /* Saving user selected options */
                UsersEvalOptions::create([
                    'option_id' => $option,
                    'users_evaluation_id' => $userEval->id,
                ]);

                /* If user choice are answer, sum points */
                if ($opt->is_answer) {
                    $this->points += $opt->item->points;
                }
            }

            $userEval->total_points = $this->points;
            $userEval->save();

            \DB::commit();

            $this->error = false;
            $this->evalEnded = true;

        }catch (\Exception $e){
            \DB::rollBack();

            $this->evalEnded = false;
        }

        $this->dispatchBrowserEvent('GoToTop');
    }

    public function render()
    {
        return view('livewire.front.course.module.evaluation');
    }
}
