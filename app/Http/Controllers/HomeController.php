<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\Courses;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        if ($request->searchTerm) {

            $courses = Courses::whereRaw('UPPER(title) LIKE "%'.strtoupper($request->searchTerm).'%"')->get();
        }else{
            $courses = Courses::all();
        }
        //dd($request->all());
        $categories = Categories::all();

        return view('front.index', compact('courses', 'categories'));
    }
}
