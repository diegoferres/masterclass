<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Courses;
use App\Models\Lessons;
use App\Models\UserLessons;
use Illuminate\Http\Request;

class CoursesController extends Controller
{
    public function course($slug)
    {
        if (!$course = Courses::where('slug', '=', $slug)->firstOrFail()) {
            dd('A');
        }

        return view('front.course', compact('course'));
    }

    public function lesson($course, $lesson)
    {
        if (!$lesson = Lessons::where('slug', '=', $lesson)->firstOrFail()) {
            return redirect()->back()->with('error', 'No existe el curso indicado');
        }

        return view('front.lesson', compact('lesson'));
    }


    public function lessonFinish(Lessons $lesson)
    {
        if (!$lesson) {
            return redirect()->back();
        }

        $userlesson = UserLessons::create(['lesson_id' => $lesson->id, 'user_id' => \Auth::id(), 'completed' => true]);

        return redirect()->back();
    }
}
