<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function index()
    {
        $courses = \Auth::user()->courses;
        $orders = \Auth::user()->courses;

        return view('front.profile.index', compact('courses'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'password' => 'sometimes|confirmed',
        ]);

        \Auth::user()->update([
           'name' =>  $request->name
        ]);

        if ($request->password) {
            \Auth::user()->update([
                'password' =>  \Hash::make($request->password)
            ]);
        }

        return redirect()->back()->with('success', 'Notificacion de prueba');
    }
}
