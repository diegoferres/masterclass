<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Mail\BuyNotification;
use App\Models\Courses;
use App\Models\Orders;
use App\Services\BancardService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function checkout($course)
    {
        $course = Courses::where('slug', $course)->first();

        if (\Auth::user()->orders->where('course_id', $course->id)->first()) {
            return redirect()->route('home')->with('error', 'Ya has comprado este curso');
        }

        return view('front.store.checkout', compact('course'));
    }

    public function insertPayment(Courses $course)
    {
        try {
            \DB::beginTransaction();
            $order = new Orders();
            $order->amount      = $course->price;
            $order->paid        = false;
            $order->course_id   = $course->id;
            $order->user_id     = \Auth::id();
            $order->payment_method_id     = 1;
            $order->save();
            \Log::debug('OrdersController@insertPayment - Order ID: '.$order->id);

            $bancard = new BancardService();
            $result = $bancard->singleBuy($order);

            \DB::commit();

            if ($result->status == 'success') {

                \Log::debug('OrdersController@insertPayment - bancard->status = success');
                return view('front.store.bancard_form', compact('result'));
            }else{
                \Log::debug('OrdersController@insertPayment - bancard->status = fail');
                return redirect()->route('order.checkout', $course->slug)->with('error', 'No se pudo procesar la orden de pago');
            }

        }catch (\Exception $e){
            dd($e->getMessage());
            \DB::rollBack();
        }
    }

    public function confirmPayment(Request $request)
    {
        try {
            if ($order = Orders::find($request->operation['shop_process_id'])) {
                \Log::debug('OrdersController@confirmPayment - Order ID: '.$order->id);
                $order->payment = $request->operation;
                if ($request->operation['response_code'] == '00') {
                    $order->paid = true;
                }else {
                    $order->paid = false;
                }
                $order->save();

                return response()->json(['success' => true]);
            }

            return response()->json(['success' => false]);

        } catch (\Exception $e) {
            \Log::error('OrdersController@confirmPayment - Line: '.$e->getLine() .' Message: '.$e->getMessage());

            return response()->json(['success' => false]);
        }
    }

    public function finish($order)
    {
        $order = Orders::find($order);
        \Log::debug('OrdersController@finish - order: '.json_encode($order));
        try {
            if ($order->payment_method_id == 1) {

                if ($order->paid) {
                    //\Mail::to($order->user->email)->send(new BuyNotification($order));
                    return view('front.store.finish', compact('order'));
                }else {
                    return redirect()->route('order.checkout', $order->course->slug)
                        ->with('bancard_response', json_encode([
                            'response_description' => json_decode($order->payment)['response_description'],
                            'extended_response_description' => json_decode($order->payment)['extended_response_description'],
                        ]));
                }
            }else {
                return view('front.store.finish', compact('order'));
            }
        }catch (\Exception $e){
            //dd('11');

            \Log::error('OrdersController@finish - Line: '.$e->getLine() .' Message: '.$e->getMessage());

            return redirect()->route('order.checkout', $order->course->slug)->with('error', 'No se pudo procesar el pago');
        }
    }

    public function rollbackPayment(Orders $order)
    {
        $bancard = new BancardService();
        $bancard->rollbackPayment($order);
    }

    public function getPayment(Orders $order)
    {
        $bancard = new BancardService();


        return $bancard->getPayment($order);
    }
}
