<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\LessonFiles;
use App\Models\Lessons;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LessonsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        //dd($request->all());
        try {
            DB::beginTransaction();

            $lesson = Lessons::create($request->only(['title', 'description', 'module_id']));

            $filename = basename($request->lesson_resource->store('public/courses/'.$request->course_id.'/lessons/' . $lesson->id . '/'));
            $lesson->resource_path  = 'courses/'.$request->course_id.'/lessons/' . $lesson->id . '/' . $filename;
            $lesson->sort           = Lessons::where('module_id', $request->module_id)->orderByDesc('sort')->first()->sort + 1;
            $lesson->save();

            if ($request->has('resources')) {
                foreach ($request->resources as $resource) {
                    $resourceFile = new LessonFiles();
                    $filename = basename($resource->store('public/courses/'.$request->course_id.'/lessons/'.$lesson->id.'/resources/'));
                    $resourceFile->filepath = 'courses/'.$request->course_id.'/lessons/' . $lesson->id . '/resources/' . $filename;
                    $resourceFile->filename = $resource->getClientOriginalName();
                    $resourceFile->lesson_id = $lesson->id;
                    $resourceFile->save();
                }
            }

            DB::commit();

            return redirect()->back()->with('notification', json_encode(['type' => 'success', 'message' => 'Datos guardados']));

        } catch (\Exception $e) {
            DB::rollBack();
            dd($e->getMessage());
            return redirect()->back()->with('notification', json_encode(['type' => 'error', 'message' => $e->getMessage()]));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Lessons  $lessons
     * @return \Illuminate\Http\Response
     */
    public function show(Lessons $lessons)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Lessons  $lessons
     * @return \Illuminate\Http\Response
     */
    public function edit(Lessons $lessons)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Lessons  $lessons
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lessons $lesson)
    {
        dd($lesson);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lessons  $lessons
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lessons $lessons)
    {
        //
    }
}
