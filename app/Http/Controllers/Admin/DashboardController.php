<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Courses;
use App\Models\Orders;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $students   = User::role('student')->get();

        $users      = User::whereHas('orders')->role('student')->get();
        $courses    = Courses::all();
        $payments   = Orders::where('paid', true)->get();

        return view('administration.index', compact('students', 'courses', 'payments'));
    }
}
