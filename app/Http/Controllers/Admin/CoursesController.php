<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\Courses;
use App\Models\CourseCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Courses::all();

        return view('administration.courses.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Categories::pluck('description', 'id');
        return view('administration.courses.form', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

        $request->validate([
            'title' => 'required|string',
            'categories' => 'required',
            'datetime_start' => 'nullable|date|after_or_equal:tomorrow',
            'datetime_end' => 'nullable|date|after_or_equal:datetime_start',
            'price' => 'required|numeric',
            'intro_resource' => 'required|image',
            'description' => 'required',
        ], [], [
            'title' => 'Titulo',
            'categories' => 'Categorias',
            'datetime_start' => 'Fecha/Hora Inicio',
            'datetime_end' => 'Fecha/Hora Fin',
            'price' => 'Precio',
            'intro_resource' => 'Imagen',
            'description' => 'Descripción',
        ]);

        //dd($request->all());
        try {
            DB::beginTransaction();

            $course = Courses::create($request->only(['title', 'description', 'price']));

            $course->datetime_start = $request->datetime_start;
            $course->datetime_end = $request->datetime_end;
            $course->save();

            foreach ($request->categories as $category) {
                (new CourseCategory())->create(['course_id' => $course->id, 'category_id' => $category]);
            }

            $filename = basename($request->intro_resource->store('public/courses/' . $course->id . '/'));
            $course->intro_resource = 'courses/' . $course->id . '/' . $filename;
            $course->save();

            DB::commit();

            return redirect()->route('admin.courses.edit', $course->id)->with('notification', json_encode(['type' => 'success', 'message' => 'Curso creado']));

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error('Admin\CoursesController@store Line: '.$e->getLine().' - Message: '.$e->getMessage());
            return redirect()->back()->with('notification', json_encode(['type' => 'error', 'message' => 'Ocurrió un error al crear']));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Courses $courses
     * @return \Illuminate\Http\Response
     */
    public function show(Courses $courses)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Courses $courses
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(Courses $course)
    {
        $categories = Categories::pluck('description', 'id');

        return view('administration.courses.edit', compact('course', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Courses $course
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Courses $course)
    {
        try {
            DB::beginTransaction();

            if ($request->has('save_options')) {
                $course->datetime_start = $request->course_datetime_start;
                $course->datetime_end = $request->course_datetime_end;
                $course->price = $request->course_price;
                $course->save();

                if ($request->categories) {
                    CourseCategory::where('course_id', $course->id)->delete();
                    /*foreach (CourseCategory::where('course_id', $course->id)->get() as $item) {
                        $item->delete();
                    }*/

                    foreach ($request->categories as $key => $category) {
                        (new CourseCategory())
                            ->create([
                                'course_id' => $course->id,
                                'category_id' => $category
                            ]);
                    }
                }
            } elseif ($request->has('save_course')) {
                $course->title = $request->course_title;
                $course->description = $request->course_description;

                /*
                 * Store course file
                 */
                if ($request->course_resourceNew) {
                    if (Storage::exists('public/'.$course->intro_resource)) {
                        Storage::delete('public/'.$course->intro_resource);
                    }

                    $filename = basename($request->course_resourceNew->store('public/courses/' . $course->id . '/'));
                    $course->intro_resource = 'courses/' . $course->id . '/' . $filename;

                }
                $course->save();

            }


            DB::commit();


            return redirect()->back()->with('notification', json_encode(['type' => 'success', 'message' => 'Cambios guardados']));;

        } catch (\Exception $e) {
            DB::rollBack();
            dd($e->getMessage() . ': ' . $e->getLine());
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Courses $courses
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Courses $course)
    {
        try {
            DB::beginTransaction();

            if (Storage::exists('public/courses/' . $course->id)) {
                Storage::deleteDirectory('public/courses/' . $course->id);
            }

            $course->delete();

            DB::commit();

            return redirect()->back()->with('notification', json_encode(['type' => 'success', 'message' => 'Curso eliminado']));

        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('notification', json_encode(['type' => 'error', 'message' => 'Ha ocurrido un error']));
        }
    }
}
