<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class UsersController extends Controller
{
    public function index()
    {
        return view('administration.users.index');
    }

    public function create()
    {
        $roles = Role::pluck('name', 'id');
        return view('administration.users.form', compact('roles'));
    }

    public function edit(User $user)
    {
        $roles = Role::pluck('name', 'id');
        return view('administration.users.form', compact('user', 'roles'));
    }
}
