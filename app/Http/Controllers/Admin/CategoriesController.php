<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Categories::all();

        return view('administration.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('administration.categories.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $category = Categories::create($request->all());

            DB::commit();

            return redirect()->route('admin.categories.index')->with('notification', json_encode(['type' => 'success', 'message' => 'Datos guardados']));

        }catch (\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('notification', json_encode(['type' => 'error', 'message' => 'Ocurrió un error']));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function show(Categories $categories)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Categories  $categories
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(Categories $categories)
    {
        $category = $categories->first();

        //dd($category);

        return view('administration.categories.form', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Categories  $categories
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Categories $categories)
    {
        try {
            DB::beginTransaction();

            $categories->first()->update($request->only('description'));

            DB::commit();

            return redirect()->route('admin.categories.index')->with('notification', json_encode(['type' => 'success', 'message' => 'Datos guardados']));

        }catch (\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('notification', json_encode(['type' => 'error', 'message' => 'Ocurrió un error']));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Categories  $categories
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Categories $categories)
    {
        try {
            DB::beginTransaction();

            $categories->first()->delete();

            DB::commit();

            return redirect()->route('admin.categories.index')->with('notification', json_encode(['type' => 'success', 'message' => 'Datos guardados']));

        }catch (\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('notification', json_encode(['type' => 'error', 'message' => 'Ocurrió un error']));
        }
    }
}
