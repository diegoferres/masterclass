<?php

return [
    'bancard_vpos_private_key_PROD' => env('BANCARD_PRIVATE_KEY_PROD', 'default_private_key'),
    'bancard_vpos_public_key_PROD' => env('BANCARD_PUBLIC_KEY_PROD', 'default_public_key'),
    'bancard_vpos_service_url_PROD' => env('BANCARD_BASE_URL_PROD', 'default_url'),

    'bancard_vpos_private_key_DEV' => env('BANCARD_PRIVATE_KEY_DEV', 'default_private_key'),
    'bancard_vpos_public_key_DEV' => env('BANCARD_PUBLIC_KEY_DEV', 'default_public_key'),
    'bancard_vpos_service_url_DEV' => env('BANCARD_BASE_URL_DEV', 'default_url'),

    'bancard_vpos_commerce_code' => env('BANCARD_COMMERCE_CODE', 2758),
    'bancard_vpos_commerce_branch' => env('BANCARD_COMMERCE_BRANCH', 3),
    'bancard_vpos_env' => env('BANCARD_ENV', 'DEV'),
];

